@rem SPDX-FileCopyrightText: 2023 Toboter
@rem SPDX-License-Identifier: GPL-2.0-or-later

py -3.8 buildScripts/generateRenderTables.py
py -3.8 buildScripts/compileTiles.py

cl65 --cpu 65C02 -Or -Cl -C cx16.cfg -o ./build/vault.prg -t cx16 -m map.txt -Ln vice.txt src/main.c src/game.c src/level.c src/rendering.c src/input.c src/items.c src/monsters.c src/player.c src/hud.c src/utils.c src/objects.c src/inventory.c src/mapgen.c src/sound.c zsmkit.lib

rem C:\llvm-mos\bin\mos-cx16-clang -Os  -o ./build/vault.prg  src/main.c src/game.c src/level.c src/rendering.c src/input.c src/items.c src/monsters.c src/player.c src/hud.c src/utils.c src/objects.c src/inventory.c src/mapgen.c src/sound.c zsmkit.lib

if errorlevel 1 exit

cd build

x16emu -debug -keymap de -abufs 32 -joy1 -prg vault.prg -run -quality nearest

asdfasdfa
