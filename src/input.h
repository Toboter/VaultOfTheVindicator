/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __INPUT
#define __INPUT

#include <stdint.h>

#define X_INPUT_UP 0
#define X_INPUT_LEFT 1
#define X_INPUT_DOWN 2
#define X_INPUT_RIGHT 3
#define X_INPUT_Y 4
#define X_INPUT_X 5
#define X_INPUT_A 6
#define X_INPUT_B 7
#define X_INPUT_L 8
#define X_INPUT_R 9
#define X_INPUT_SELECT 10
#define X_INPUT_START 11


extern uint8_t input_inputsHeld[12];
extern uint8_t input_inputsPressed[12];

extern void input_init();
extern void input_update(uint8_t invalidateReleased);
extern uint8_t input_isKeyDown(uint8_t keycode);

#endif
