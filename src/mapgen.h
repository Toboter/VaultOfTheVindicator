/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __MAPGEN
#define __MAPGEN

#include <stdint.h>

extern void mapgen_init();
extern void mapgen_generateWorld();

#endif
