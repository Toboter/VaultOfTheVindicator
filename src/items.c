/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "items.h"

#include <stdlib.h>

#include "level.h"
#include "player.h"
#include "inventory.h"
#include "hud.h"
#include "utils.h"
#include "monsters.h"
#include "game.h"
#include "sound.h"


const uint8_t itemIDtoWallID[] = {0,1,1,1,2,3,4,4,5,6,7,8,9,10,10,10,10, 11, 12, 13, 13, 29};

const uint8_t swordStrength[5] = {0,1,2,1,3};
const uint8_t swordDefense[5] = {0,0,0,1,1};

const uint8_t armorStrength[4] = {0,1,1,2};
const uint8_t armorMagic[4] = {0,0,0,2};
const uint8_t armorArmor[4] = {1,1,2,1};



#define WAND_MAGIC_BOOST 2

char * items_itemNames[] = {
	"",
	"Farmer's Sword",
	"Student's Foil",
	"Guard's Sword",
	"Warden's Sword",
	"Leather Vest",
	"Chain Mail",
	"Guard's Breastplate",
	"Warden's Robe",
	"Cracked Focus",
	"Devout's Lilly",
	"Chapped Bone",
	"Demonic Anchor",
	"Zealot's scroll",
	"Warden's scroll",
	"Scroll of Icarion",
	"Student's scroll",
	"Guard's drought",
	"Wand of Icarion",
	"Demonic Key",
	"Golden Key",
	"Rift Key"
};

uint8_t scrollEffects[4] = {0,1,2,3};

void items_randomizeScrolls() {
	uint8_t i=0;

	scrollEffects[0] = 0;
	scrollEffects[1] = 1;
	scrollEffects[2] = 2;
	scrollEffects[3] = 3;

	utils_shuffleList(scrollEffects, 2);
}

uint8_t items_spawnItemObject(uint8_t x, uint8_t y, uint8_t itemID) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_ITEM);
	if (obj) {
		obj->wallType = itemIDtoWallID[itemID] | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
		obj->data[0] = itemID;
		return 1;
	}
	return 0;
}

void items_updateItemObject(Object * object) {
	if (level_currentObjectInteracted) {
		if (inventory_addItem(object->data[0])) {
			player_onInteract();
			sound_playSFX(SOUND_EFFECT_BOOP, SOUND_PRIORITY_ACTION);
			if (object->data[0] == ITEM_MISC_GHOST_KEY && player_progress < 2) player_progress = 2;
			hud_print(items_itemNames[object->data[0]]);
			utils_sleep(15);
			level_deleteObject(object);
		}
	}
}

void items_applyHeldEffects(uint8_t heldItemID, uint8_t equippedItemID) {
	player_itemStrength = 0;
	player_itemMagic = 0;
	player_itemArmor = 0;

	if (heldItemID) {
		if (ITEM_GET_TYPE(heldItemID) == ITEM_TYPE_SWORD) {
			player_itemStrength += swordStrength[heldItemID];
			player_itemArmor += swordDefense[heldItemID];
		} else if (heldItemID == ITEM_MISC_WAND) { //magic wand
			player_itemMagic += WAND_MAGIC_BOOST;
		}
	}

	if (equippedItemID) {
		if (ITEM_GET_TYPE(equippedItemID) == ITEM_TYPE_ARMOR) {
			equippedItemID--;
			player_itemArmor += armorArmor[equippedItemID & 0b11];
			player_itemMagic += armorMagic[equippedItemID & 0b11];
			player_itemStrength += armorStrength[equippedItemID & 0b11];
		}
	}
}

uint8_t items_useItem(uint8_t itemID) {
	uint8_t effect, x, y;
	Object * obj;
	if (itemID) {
		if (ITEM_GET_TYPE(itemID) == ITEM_TYPE_ARMOR) { //is armor
			if (inventory_getEquippedArmor() != inventory_getSelectedItem()) {
				sound_playSFX(SOUND_EFFECT_BOOP, SOUND_PRIORITY_ACTION);
				inventory_setEquippedArmor(inventory_getCursorPos());
			} else {
				sound_playSFX(SOUND_EFFECT_BOOP, SOUND_PRIORITY_ACTION);
				inventory_setEquippedArmor(INVENTORY_NO_EQUIP);
			}
			return 1;
		}

		else if (itemID == ITEM_MISC_POTION) {
			player_hp = player_maxHp;
			sound_playSFX(SOUND_EFFECT_MAGIC_ATTACK, SOUND_PRIORITY_ACTION);
			inventory_popCurrentItem();
			hud_print("Healed!");
			utils_sleep(30);
			return 1;
		}

		else if (itemID == ITEM_CRAFT_SPECIAL) { //trigger ending if not used on an object
			obj = level_loadedLevel->objects;
			y = 0;
			for (x=0; x<OBJECT_COUNT; x++, obj++) {
				if (obj->_exists && obj->_x == player_interactX && obj->_y == player_interactY && obj->_objectType != OBJECT_ID_DECORATION) {
					y = 1;
				}
			}

			if (!y && level_currentLevel != 9) {
				game_changeMode(GAME_MODE_END1);
			}
		}

		else if (ITEM_GET_TYPE(itemID) == ITEM_TYPE_SWORD) {
			sound_playSFX(SOUND_EFFECT_SWORD, SOUND_PRIORITY_ACTION);
			hud_displayEffect(HUD_EFFECT_SWORD);
			return 0;
		}

		else if (itemID == ITEM_MISC_WAND) {
			sound_playSFX(SOUND_EFFECT_MAGIC_ATTACK, SOUND_PRIORITY_ACTION);
			hud_displayEffect(HUD_EFFECT_MAGIC);
			return 0;
		}

		else if (ITEM_GET_TYPE(itemID) == ITEM_TYPE_SCROLL) {
			effect = (itemID-1) & 0b11;
			inventory_popCurrentItem();
			hud_displayEffect(HUD_EFFECT_MAGIC);
			sound_playSFX(SOUND_EFFECT_MAGIC_CAST, SOUND_PRIORITY_ACTION);
			effect = scrollEffects[effect];
			switch (effect) {

				case 0: //Teleport
					x = rand() & 31;
					y = rand() & 31;
					while (!level_getWall(x,y,WALL_UP) || (level_loadedLevel->collisionMap[x][y])) {
						x = rand() & 31;
						y = rand() & 31;
					}

					player_x = x;
					player_y = y;
					hud_print("Cast Teleport");
					break;

				case 1: //Increase Stat
					switch (rand3()) {
						case 0:
							player_baseStrength++;
							hud_print("Increased Strength");
							break;
						case 1:
							player_baseMagic++;
							hud_print("Increased Magic");
							break;
						case 2:
							player_baseArmor++;
							hud_print("Increased Defense");
							break;
					}
					break;

				case 2: //Poison & Charm
				case 3:
					if (effect == 2) {
						effect = MONSTER_EFFECT_CHARM;
						hud_print("Cast Charm");
					} else {
						effect = MONSTER_EFFECT_POISON;
						hud_print("Cast Poison");
					}
					obj = level_loadedLevel->objects;
					for (x=0; x<OBJECT_COUNT; x++, obj++) {
						if (obj->_exists && (obj->_objectType & OBJECT_TYPE_ID) == OBJECT_ID_MONSTER) {
							y = 0; //distance
							if (obj->_x < player_x) {
								y += player_x - obj->_x;
							} else {
								y += obj->_x - player_x ;
							}

							if (obj->_y < player_y) {
								y += player_y - obj->_y;
							} else {
								y += obj->_y - player_y ;
							}

							if (y <= 5) {
								monsters_applyEffect(obj, effect, 5);
								if (effect == MONSTER_EFFECT_CHARM && obj->data[MONSTER_DATA_TYPE] == MONSTER_TYPE_NECRO_BOSS) {
									game_changeMode(GAME_MODE_END3);
								}
							}
						}
					}
					break;
			}
			utils_sleep(40);
			return 1;

		} else {
			return 0;
		}
	}
}
