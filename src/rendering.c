/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "rendering.h"

//#define __CX16__
#include <cx16.h>
#include <cbm.h>

#include "utils.h"
#include "render_tables.h"
#include "level.h"
#include "input.h"

uint8_t rendering_cameraX = 14;
uint8_t rendering_cameraY = 21;
uint8_t rendering_cameraRotation = 0;

uint8_t rendering_ceilingColor = 0x45;
uint8_t rendering_distanceColor = 0x66;
uint8_t rendering_floorColor = 0x59;

uint16_t y_addr_lookup[120] = {0u, 320u, 640u, 960u, 1280u, 1600u, 1920u, 2240u, 2560u, 2880u, 3200u, 3520u, 3840u, 4160u, 4480u, 4800u, 5120u, 5440u, 5760u, 6080u, 6400u, 6720u, 7040u, 7360u, 7680u, 8000u, 8320u, 8640u, 8960u, 9280u, 9600u, 9920u, 10240u, 10560u, 10880u, 11200u, 11520u, 11840u, 12160u, 12480u, 12800u, 13120u, 13440u, 13760u, 14080u, 14400u, 14720u, 15040u, 15360u, 15680u, 16000u, 16320u, 16640u, 16960u, 17280u, 17600u, 17920u, 18240u, 18560u, 18880u, 19200u, 19520u, 19840u, 20160u, 20480u, 20800u, 21120u, 21440u, 21760u, 22080u, 22400u, 22720u, 23040u, 23360u, 23680u, 24000u, 24320u, 24640u, 24960u, 25280u, 25600u, 25920u, 26240u, 26560u, 26880u, 27200u, 27520u, 27840u, 28160u, 28480u, 28800u, 29120u, 29440u, 29760u, 30080u, 30400u, 30720u, 31040u, 31360u, 31680u, 32000u, 32320u, 32640u, 32960u, 33280u, 33600u, 33920u, 34240u, 34560u, 34880u, 35200u, 35520u, 35840u, 36160u, 36480u, 36800u, 37120u, 37440u, 37760u, 38080u};


uint8_t (*walls)[32][16][16] = (uint8_t (*)[32][16][16])0xa000;

/*uint8_t test_image[16][16] = {
	{0x05, 0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05},
	{0x05, 0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x05},
	{0x05, 0x0,0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x0,0x05},
	{0x05, 0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x0,0x05},
	{0x05, 0x0,0x0,0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x0,0x0,0x05},
	{0x05, 0x0,0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x0,0x05},
	{0x05, 0x05,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x05,0x05},
	{0x05, 0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05}
};*/

uint8_t dbg = 0;
void dbgOut() {
	VERA.address = ++dbg;
	VERA.data0 = dbg;
}

#define RENDER_DISTANCE 8

#define BITMAP_WINDOW_SIZE 100

#define BG_CLEAR_COLOR 0x11

#define SOLID_TILE_BANK 1
#define TRANS_TILE_BANK 2

//size 100
#if BITMAP_WINDOW_SIZE==100
#define BITMAP_VRAM_0 0xc9eu //buffer size 320*120 bytes ~= 2048*19
#define BITMAP_VRAM_MAP0 0x0u
#define BITMAP_VRAM_1 0xa49eu
#define BITMAP_VRAM_MAP1 0x13u
#endif

//size 120
#if BITMAP_WINDOW_SIZE==120
#define BITMAP_VRAM_0 0x14u //buffer size 320*120 bytes ~= 2048*19
#define BITMAP_VRAM_MAP0 0x0u
#define BITMAP_VRAM_1 0x9814u
#define BITMAP_VRAM_MAP1 0x13u
#endif

//size 110
#if BITMAP_WINDOW_SIZE==110
#define BITMAP_VRAM_0 0x659u //buffer size 320*120 bytes ~= 2048*19
#define BITMAP_VRAM_MAP0 0x0u
#define BITMAP_VRAM_1 0x9e59u
#define BITMAP_VRAM_MAP1 0x13u
#endif

uint8_t depthBuffer[BITMAP_WINDOW_SIZE];

uint8_t marginBuffer0[BITMAP_WINDOW_SIZE]; //contains how much black was drawn the last frame1
uint8_t marginBuffer1[BITMAP_WINDOW_SIZE]; //contains how much black was drawn the last frame2

uint8_t currentBuffer = 0; //which buffer is the screen written to rn?
uint8_t * marginBuffer;
uint16_t layer0BaseAddress;// = 320*10+30;

uint8_t maxI;

uint16_t wallPixelIncrement, wallYIncr;
uint16_t wallHeightDecrement;
uint16_t wallX, wallY;
uint8_t wallXAbs;
uint16_t wallHeight;
uint8_t wallStart;
uint8_t wallStop;
int8_t currentWallX1;
int8_t currentWallX2;
uint8_t margin;


void rendering_initRendering() {
	VERA.layer0.config = 0b00000111;//bitmap mode, color 8bpp#
	VERA.layer0.tilebase = 0b00;//bitmap addr 0, width 320x200

	SET_CTRL(0, 0);
	VERA.display.video |= 0b00010000;//enable  layer0
	VERA.display.video &= ~0b00100000;//disable layer1
	VERA.display.hscale = 32; //set divider to 160x120
	VERA.display.vscale = 32;

	//clear bitmap vram
	rendering_resetBuffers();

	rendering_flipBuffers(); //set up double buffering properly;

}

/*
* Wall rendering functions (currentWallX = offset on the screen, pX/pY = absolute tile offset from camera, wallType = byte from level file, transparent = are we rendering transparent walls rn?)
*
*/



void renderLeftWall(int8_t currentWallX, uint8_t pX, uint8_t pY, uint8_t wallType, uint8_t transparent) {
	register uint8_t j;
	register uint8_t (*wall)[16][16];

	if (!(wallType & WALL_FLAG_TYPE)) return;
	if ((wallType & WALL_FLAG_TRANSPARENT) ^ transparent) return;

	SET_RAM_BANK(transparent ? TRANS_TILE_BANK : SOLID_TILE_BANK);

	wallType &= WALL_FLAG_TYPE;

	wall = &((*walls)[wallType]);

	wallPixelIncrement = orthoWallPixelIncrement[pX][pY];

	wallHeightDecrement = wallGradient[pX];

	wallHeight = ((uint16_t)parallelWallSize[pY])<<8;

	wallX = 0;
	while (wallX < 0x1000) {

		if (currentWallX >= 0 && currentWallX < BITMAP_WINDOW_SIZE && depthBuffer[currentWallX]>=pY+pX) {
			depthBuffer[currentWallX] = pY+pX;

			wallStart = (BITMAP_WINDOW_SIZE/2 - (wallHeight>>9));

			wallStop = BITMAP_WINDOW_SIZE-wallStart;

			margin = marginBuffer[currentWallX];
			if (margin > wallStart) margin = wallStart;

			VERA.address = layer0BaseAddress + currentWallX + y_addr_lookup[margin];
			VERA.address_hi = 0b11100000;
			if (VERA.address < layer0BaseAddress) VERA.address_hi |= 1;

			for (j=margin;j<wallStart;j++) {
				VERA.data0 = rendering_ceilingColor;
			}

			wallYIncr = wallHeightToPixelIncrement[(wallHeight>>8)];

			wallXAbs = wallX>>8;
			wallY = 0x0;
			for (j=wallStart;j<=wallStop;j++) {
				VERA.data0 = (*wall)[wallXAbs][wallY>>8];
				wallY += wallYIncr;
			}

			for (j=wallStop;j<BITMAP_WINDOW_SIZE - margin;j++) {
				VERA.data0 = rendering_floorColor;
			}

			marginBuffer[currentWallX] = wallStart;
		}

		wallX += wallPixelIncrement;
		wallHeight -= wallHeightDecrement;
		currentWallX ++;
	}
}

void renderCentralWall(int8_t currentWallX, uint8_t pX, uint8_t pY, uint8_t wallType, uint8_t transparent) {
	register uint8_t j;
	register uint8_t (*wall)[16][16];

	uint16_t wallEnd = 0x1000;

	if (!(wallType & WALL_FLAG_TYPE)) return;
	if ((wallType & WALL_FLAG_TRANSPARENT) ^ transparent) return;
	wallType &= WALL_FLAG_TYPE;
	wall = &((*walls)[wallType]);

	SET_RAM_BANK(transparent ? TRANS_TILE_BANK : SOLID_TILE_BANK);

	wallHeight = ((uint16_t)parallelWallSize[pY+1])<<8;

	wallYIncr = wallHeightToPixelIncrement[wallHeight>>8];
	wallX = 0x0;

	if (transparent) wallEnd = 0x0f00;

	while(wallX < wallEnd) {
		if (currentWallX >= 0 && currentWallX < BITMAP_WINDOW_SIZE && depthBuffer[currentWallX]>=pY+pX) {
			depthBuffer[currentWallX] = pY+pX;

			wallStart = (BITMAP_WINDOW_SIZE/2 - (wallHeight>>9));

			wallStop = BITMAP_WINDOW_SIZE-wallStart;

			margin = marginBuffer[currentWallX];
			if (margin > wallStart) margin = wallStart;

			VERA.address = layer0BaseAddress + currentWallX + y_addr_lookup[margin];
			VERA.address_hi = 0b11100000;
			if (VERA.address < layer0BaseAddress) VERA.address_hi |= 1;

			wallY = 0;

			for (j=margin;j<wallStart;j++) {
				VERA.data0 = rendering_ceilingColor;
			}

			wallY = 0x00;
			wallXAbs = wallX>>8;
			for (j=wallStart;j<=wallStop;j++) {
				VERA.data0 = (*wall)[wallXAbs][wallY>>8];
				wallY += wallYIncr;
			}

			for (j=wallStop;j<BITMAP_WINDOW_SIZE - margin;j++) {
				VERA.data0 = rendering_floorColor;
			}

			marginBuffer[currentWallX] = wallStart;
		}

		wallX += wallYIncr;
		currentWallX ++;
	}
}

void renderRightWall(int8_t currentWallX, uint8_t pX, uint8_t pY, uint8_t wallType, uint8_t transparent) {
	register uint8_t j;
	register uint8_t (*wall)[16][16];


	if (!(wallType & WALL_FLAG_TYPE)) return;
	if ((wallType & WALL_FLAG_TRANSPARENT) ^ transparent) return;

	wallType &= WALL_FLAG_TYPE;

	wall = &((*walls)[wallType]);

	SET_RAM_BANK(transparent ? TRANS_TILE_BANK : SOLID_TILE_BANK);

	wallPixelIncrement = orthoWallPixelIncrement[pX][pY];

	wallHeightDecrement = wallGradient[pX];

	wallHeight = ((uint16_t)parallelWallSize[pY+1])<<8;
	//wallHeight -= 0x300;

	wallX = 0x0;
	while (wallX < 0x1000) {

		if (currentWallX >= 0 && currentWallX < BITMAP_WINDOW_SIZE && depthBuffer[currentWallX]>=pY+pX) {
			depthBuffer[currentWallX] = pY+pX;

			wallStart = (BITMAP_WINDOW_SIZE/2 - (wallHeight>>9));

			wallStop = BITMAP_WINDOW_SIZE-wallStart;

			margin = marginBuffer[currentWallX];
			if (margin > wallStart) margin = wallStart;

			VERA.address = layer0BaseAddress + currentWallX + y_addr_lookup[margin];
			VERA.address_hi = 0b11100000;
			if (VERA.address < layer0BaseAddress) VERA.address_hi |= 1;

			for (j=margin;j<wallStart;j++) {
				VERA.data0 = rendering_ceilingColor;
			}

			wallYIncr = wallHeightToPixelIncrement[(wallHeight>>8)];

			wallXAbs = wallX>>8;
			wallY = 0x0;
			for (j=wallStart;j<=wallStop;j++) {
				VERA.data0 = (*wall)[wallXAbs][wallY>>8];
				wallY += wallYIncr;
			}

			for (j=wallStop;j<BITMAP_WINDOW_SIZE - margin;j++) {
				VERA.data0 = rendering_floorColor;
			}

			marginBuffer[currentWallX] = wallStart;
		}

		wallX += wallPixelIncrement;
		wallHeight += wallHeightDecrement;
		currentWallX ++;
	}
}

/*
* Full rendering functions per directions
*
*/

uint8_t tmp;

void rendering_renderUp() {
	uint8_t i,j,k, y;
	uint8_t transparency = 0;
	//VERA.address_hi = 0b11100000;
	//renderCentralWall(0, 0);

	maxI = rendering_cameraY; //prevent oob rendering
	if (maxI > RENDER_DISTANCE) maxI = RENDER_DISTANCE;

	i=0;
	while(i != 255) { //render in two passes: first front to back solid, then back to front transparent
		//input_update(0);
		y = rendering_cameraY - i;

		//left
		currentWallX1 = i&1 ? 0 : -((parallelWallSize[i+1]>>1));// : 0;
		currentWallX2 = i&1 ? -(parallelWallSize[i]>>1) : -parallelWallSize[i];

		for (j=(i>>1) + 1; j>0; j--) {
			if (j <= rendering_cameraX) {
				renderCentralWall(currentWallX1, j, i, level_getWall(rendering_cameraX - j, y, WALL_UP), transparency);
				renderLeftWall(currentWallX2, j , i, level_getWall(rendering_cameraX - j, y, WALL_LEFT), transparency);
			}
			currentWallX1 += parallelWallSize[i+1];
			currentWallX2 += parallelWallSize[i];

		}


		//center
		renderCentralWall(BITMAP_WINDOW_SIZE/2 - parallelWallSize[i+1]/2, 0, i, level_getWall(rendering_cameraX, y, WALL_UP), transparency);

		renderLeftWall(BITMAP_WINDOW_SIZE/2 - parallelWallSize[i]/2, 0,i, level_getWall(rendering_cameraX, y, WALL_LEFT), transparency);

		renderRightWall(BITMAP_WINDOW_SIZE/2 + parallelWallSize[i+1]/2, 0,i, level_getWall(rendering_cameraX, y, WALL_RIGHT), transparency);

		//right
		currentWallX1 = i&1 ? BITMAP_WINDOW_SIZE - (parallelWallSize[i+1]) : BITMAP_WINDOW_SIZE - (parallelWallSize[i+1]>>1);// : 0;
		currentWallX2 = i&1 ? BITMAP_WINDOW_SIZE : BITMAP_WINDOW_SIZE + (parallelWallSize[i+1]>>1);

		for (j=(i>>1) + 1; j>0; j--) {
			if (j + rendering_cameraX < LEVEL_SIZE ) {
				renderCentralWall(currentWallX1, j, i, level_getWall(rendering_cameraX + j, y, WALL_UP), transparency);
				renderRightWall(currentWallX2, j , i, level_getWall(rendering_cameraX + j, y, WALL_RIGHT), transparency);
			}
			currentWallX1 -= parallelWallSize[i+1];
			currentWallX2 -= parallelWallSize[i+1];

		}

		if (i >= maxI) {
			for (k=0; k< BITMAP_WINDOW_SIZE; k++) {
				if (depthBuffer[k] == 0xff) {
					VERA.address_hi = 0b11100000;
					VERA.address = layer0BaseAddress + k + y_addr_lookup[marginBuffer[k]];
					if (VERA.address < layer0BaseAddress) VERA.address_hi |= 1;
					for (j=marginBuffer[k];j<=(BITMAP_WINDOW_SIZE- parallelWallSize[RENDER_DISTANCE])>>1;j++) VERA.data0=rendering_ceilingColor;
					for (j=0;j<parallelWallSize[RENDER_DISTANCE];j++) VERA.data0=rendering_distanceColor;

					for (j=marginBuffer[k];j<=(BITMAP_WINDOW_SIZE- parallelWallSize[RENDER_DISTANCE])>>1;j++) VERA.data0=rendering_floorColor;
					marginBuffer[k] = 0;
				}
			}

			transparency = WALL_FLAG_TRANSPARENT;
			SET_CTRL(2, 0);
			VERA.display.fxctrl |= 0b10000000; //transparent writes on
		}

		if (transparency) --i;
		else ++i;
	}
}

uint8_t i;
void rendering_renderDown() {
	uint8_t j,k,  y;
	uint8_t transparency = 0;

	maxI = LEVEL_SIZE - rendering_cameraY - 1; //prevent oob rendering
	if (maxI > RENDER_DISTANCE) maxI = RENDER_DISTANCE;

	VERA.address_hi = 0b11100000;
	i=0;

	while(i != 255) { //render in two passes: first front to back solid, then back to front transparent
		y = rendering_cameraY + i;
		//left
		currentWallX1 = i&1 ? 0 : -((parallelWallSize[i+1]>>1));// : 0;
		currentWallX2 = i&1 ? -(parallelWallSize[i]>>1) : -parallelWallSize[i];

		for (j=(i>>1) + 1; j>0; j--) {
			if (j + rendering_cameraX < LEVEL_SIZE) {
				renderCentralWall(currentWallX1, j, i, level_getWall(rendering_cameraX + j, y, WALL_DOWN), transparency);
				renderLeftWall(currentWallX2, j , i, level_getWall(rendering_cameraX + j, y, WALL_RIGHT), transparency);
			}
			currentWallX1 += parallelWallSize[i+1];
			currentWallX2 += parallelWallSize[i];

		}

		//center
		renderCentralWall(BITMAP_WINDOW_SIZE/2 - parallelWallSize[i+1]/2, 0, i, level_getWall(rendering_cameraX, y, WALL_DOWN), transparency);

		renderLeftWall(BITMAP_WINDOW_SIZE/2 - parallelWallSize[i]/2, 0,i, level_getWall(rendering_cameraX, y, WALL_RIGHT), transparency);

		renderRightWall(BITMAP_WINDOW_SIZE/2 + parallelWallSize[i+1]/2, 0,i, level_getWall(rendering_cameraX, y, WALL_LEFT), transparency);


		//right
		currentWallX1 = i&1 ? BITMAP_WINDOW_SIZE - (parallelWallSize[i+1]) : BITMAP_WINDOW_SIZE - (parallelWallSize[i+1]>>1);// : 0;
		currentWallX2 = i&1 ? BITMAP_WINDOW_SIZE : BITMAP_WINDOW_SIZE + (parallelWallSize[i+1]>>1);

		for (j=(i>>1) + 1; j>0; j--) {
			if (j <= rendering_cameraX) {
				renderCentralWall(currentWallX1, j, i, level_getWall(rendering_cameraX - j, y, WALL_DOWN), transparency);
				renderRightWall(currentWallX2, j , i, level_getWall(rendering_cameraX - j, y, WALL_LEFT), transparency);

			}
			currentWallX1 -= parallelWallSize[i+1];
			currentWallX2 -= parallelWallSize[i+1];

		}

		if (i >= maxI) {
			for (k=0; k< BITMAP_WINDOW_SIZE; k++) {
				if (depthBuffer[k] == 0xff) {
					VERA.address_hi = 0b11100000;
					VERA.address = layer0BaseAddress + k + y_addr_lookup[marginBuffer[k]];
					if (VERA.address < layer0BaseAddress) VERA.address_hi |= 1;
					for (j=marginBuffer[k];j<=(BITMAP_WINDOW_SIZE- parallelWallSize[RENDER_DISTANCE])>>1;j++) VERA.data0=rendering_ceilingColor;
					for (j=0;j<parallelWallSize[RENDER_DISTANCE];j++) VERA.data0=rendering_distanceColor;
					for (j=marginBuffer[k];j<=(BITMAP_WINDOW_SIZE- parallelWallSize[RENDER_DISTANCE])>>1;j++) VERA.data0=rendering_floorColor;
					marginBuffer[k] = 0;

				}
			}

			transparency = WALL_FLAG_TRANSPARENT;
			SET_CTRL(2, 0);
			VERA.display.fxctrl |= 0b10000000; //transparent writes on
		}

		if (transparency) --i;
		else ++i;
	}
}

void rendering_renderRight() {
	uint8_t i,j,k,  x;
	uint8_t transparency = 0;

	maxI = LEVEL_SIZE - rendering_cameraX - 1; //prevent oob rendering
	if (maxI > RENDER_DISTANCE) maxI = RENDER_DISTANCE;

	VERA.address_hi = 0b11100000;
	i=0;
	while(i != 255) { //render in two passes: first front to back solid, then back to front transparent
		x = rendering_cameraX + i;

		//left
		currentWallX1 = i&1 ? 0 : -((parallelWallSize[i+1]>>1));// : 0;
		currentWallX2 = i&1 ? -(parallelWallSize[i]>>1) : -parallelWallSize[i];

		for (j=(i>>1) + 1; j>0; j--) {
			if (j <= rendering_cameraY) {
				renderCentralWall(currentWallX1, j, i, level_getWall(x, rendering_cameraY - j, WALL_RIGHT), transparency);
				renderLeftWall(currentWallX2, j , i, level_getWall(x, rendering_cameraY - j, WALL_UP), transparency);
			}
			currentWallX1 += parallelWallSize[i+1];
			currentWallX2 += parallelWallSize[i];

		}

		//center
		renderCentralWall(BITMAP_WINDOW_SIZE/2 - parallelWallSize[i+1]/2, 0, i, level_getWall(x, rendering_cameraY, WALL_RIGHT), transparency);
		renderLeftWall(BITMAP_WINDOW_SIZE/2 - parallelWallSize[i]/2, 0,i, level_getWall(x, rendering_cameraY, WALL_UP), transparency);
		renderRightWall(BITMAP_WINDOW_SIZE/2 + parallelWallSize[i+1]/2, 0,i, level_getWall(x, rendering_cameraY, WALL_DOWN), transparency);


		//right
		currentWallX1 = i&1 ? BITMAP_WINDOW_SIZE - (parallelWallSize[i+1]) : BITMAP_WINDOW_SIZE - (parallelWallSize[i+1]>>1);// : 0;
		currentWallX2 = i&1 ? BITMAP_WINDOW_SIZE : BITMAP_WINDOW_SIZE + (parallelWallSize[i+1]>>1);

		for (j=(i>>1) + 1; j>0; j--) {
			if (j + rendering_cameraY < LEVEL_SIZE ) {
				renderCentralWall(currentWallX1, j, i, level_getWall(x, rendering_cameraY + j, WALL_RIGHT), transparency);
				renderRightWall(currentWallX2, j , i, level_getWall(x, rendering_cameraY + j, WALL_DOWN), transparency);
			}
			currentWallX1 -= parallelWallSize[i+1];
			currentWallX2 -= parallelWallSize[i+1];

		}

		if (i >= maxI) {
			for (k=0; k< BITMAP_WINDOW_SIZE; k++) {
				if (depthBuffer[k] == 0xff) {
					VERA.address_hi = 0b11100000;
					VERA.address = layer0BaseAddress + k + y_addr_lookup[marginBuffer[k]];
					if (VERA.address < layer0BaseAddress) VERA.address_hi |= 1;
					for (j=marginBuffer[k];j<=(BITMAP_WINDOW_SIZE- parallelWallSize[RENDER_DISTANCE])>>1;j++) VERA.data0=rendering_ceilingColor;
					for (j=0;j<parallelWallSize[RENDER_DISTANCE];j++) VERA.data0=rendering_distanceColor;
					for (j=marginBuffer[k];j<=(BITMAP_WINDOW_SIZE- parallelWallSize[RENDER_DISTANCE])>>1;j++) VERA.data0=rendering_floorColor;
					marginBuffer[k] = 0;
				}
			}

			transparency = WALL_FLAG_TRANSPARENT;
			SET_CTRL(2, 0);
			VERA.display.fxctrl |= 0b10000000; //transparent writes on
		}

		if (transparency) --i;
		else ++i;
	}
}

void rendering_renderLeft() {
	uint8_t i,j,k,  x;
	uint8_t transparency = 0;

	maxI = rendering_cameraX; //prevent oob rendering
	if (maxI > RENDER_DISTANCE) maxI = RENDER_DISTANCE;

	VERA.address_hi = 0b11100000;
	i=0;
	while(i != 255) { //render in two passes: first front to back solid, then back to front transparent
		x = rendering_cameraX - i;

		//left
		currentWallX1 = i&1 ? 0 : -((parallelWallSize[i+1]>>1));// : 0;
		currentWallX2 = i&1 ? -(parallelWallSize[i]>>1) : -parallelWallSize[i];

		for (j=(i>>1) + 1; j>0; j--) {
			if (j + rendering_cameraY < LEVEL_SIZE) {
				renderCentralWall(currentWallX1, j, i, level_getWall(x, rendering_cameraY + j, WALL_LEFT), transparency);
				renderLeftWall(currentWallX2, j , i, level_getWall(x, rendering_cameraY + j, WALL_DOWN), transparency);

			}
			currentWallX1 += parallelWallSize[i+1];
			currentWallX2 += parallelWallSize[i];

		}

		//center
		renderCentralWall(BITMAP_WINDOW_SIZE/2 - parallelWallSize[i+1]/2, 0, i, level_getWall(x, rendering_cameraY, WALL_LEFT), transparency);
		renderLeftWall(BITMAP_WINDOW_SIZE/2 - parallelWallSize[i]/2, 0,i, level_getWall(x, rendering_cameraY, WALL_DOWN), transparency);
		renderRightWall(BITMAP_WINDOW_SIZE/2 + parallelWallSize[i+1]/2, 0,i, level_getWall(x, rendering_cameraY, WALL_UP), transparency);


		//right
		currentWallX1 = i&1 ? BITMAP_WINDOW_SIZE - (parallelWallSize[i+1]) : BITMAP_WINDOW_SIZE - (parallelWallSize[i+1]>>1);// : 0;
		currentWallX2 = i&1 ? BITMAP_WINDOW_SIZE : BITMAP_WINDOW_SIZE + (parallelWallSize[i+1]>>1);

		for (j=(i>>1) + 1; j>0; j--) {
			if (j <= rendering_cameraY) {
				renderCentralWall(currentWallX1, j, i, level_getWall(x, rendering_cameraY - j, WALL_LEFT), transparency);
				renderRightWall(currentWallX2, j , i, level_getWall(x, rendering_cameraY - j, WALL_UP), transparency);

			}
			currentWallX1 -= parallelWallSize[i+1];
			currentWallX2 -= parallelWallSize[i+1];

		}

		if (i >= maxI) {
			for (k=0; k< BITMAP_WINDOW_SIZE; k++) {
				if (depthBuffer[k] == 0xff) {
					VERA.address_hi = 0b11100000;
					VERA.address = layer0BaseAddress + k + y_addr_lookup[marginBuffer[k]];
					if (VERA.address < layer0BaseAddress) VERA.address_hi |= 1;
					for (j=marginBuffer[k];j<=(BITMAP_WINDOW_SIZE- parallelWallSize[RENDER_DISTANCE])>>1;j++) VERA.data0=rendering_ceilingColor;
					for (j=0;j<parallelWallSize[RENDER_DISTANCE];j++) VERA.data0=rendering_distanceColor;
					for (j=marginBuffer[k];j<=(BITMAP_WINDOW_SIZE- parallelWallSize[RENDER_DISTANCE])>>1;j++) VERA.data0=rendering_floorColor;
					marginBuffer[k] =0;
				}
			}

			transparency = WALL_FLAG_TRANSPARENT;
			SET_CTRL(2, 0);
			VERA.display.fxctrl |= 0b10000000; //transparent writes on
		}

		if (transparency) --i;
		else ++i;
	}

}

/*
*
* main rendering functions
*
*/




void rendering_renderBitmap() {
	uint8_t i;
	//clear depth buffer
	for (i=0; i<BITMAP_WINDOW_SIZE; i++) depthBuffer[i] = 0xff;

	switch(rendering_cameraRotation) {
		case WALL_UP:
			rendering_renderUp();
			break;
		case WALL_DOWN:
			rendering_renderDown();
			break;
		case WALL_LEFT:
			rendering_renderLeft();
			break;
		case WALL_RIGHT:
			rendering_renderRight();
			break;
	}
	SET_RAM_BANK(level_currentLevelBank);

	VERA.display.fxctrl &= 0b01111111;//transparent writes off

	rendering_flipBuffers();
}

void rendering_flipBuffers() {
	if (!currentBuffer) {
		VERA.layer0.tilebase = BITMAP_VRAM_MAP0<<2; //display buffer 0
		layer0BaseAddress = BITMAP_VRAM_1; //write to buffer 1
		marginBuffer = marginBuffer1;
		currentBuffer = 1;
	} else {
		VERA.layer0.tilebase = BITMAP_VRAM_MAP1<<2; //display buffer 1
		layer0BaseAddress = BITMAP_VRAM_0; //write to buffer 0
		marginBuffer = marginBuffer0;
		currentBuffer = 0;
	}
}

void rendering_resetBuffers() {
	uint16_t i;
	VERA.address = 0;
	VERA.address_hi = 0b00010000;
	for (i=0;i<0xffffu;i++) {
		VERA.data0 = BG_CLEAR_COLOR;
		if ((i&0x7ff) == 0)utils_sleep(1);
	}
	for (i=0;i<0x3001;i++) {
		VERA.data0 = BG_CLEAR_COLOR;
		if ((i&0x7ff) == 0)utils_sleep(1);
	}

	for (i=0; i<BITMAP_WINDOW_SIZE; i++) {
		marginBuffer0[i] = 0;
		marginBuffer1[i] = 0;
	}
}

void rendering_renderScreen(char * screenName) {
	cbm_k_setnam(screenName);
	cbm_k_setlfs(0, 8, 0);
	cbm_k_load(2, 0);
	cbm_k_load(2, BITMAP_VRAM_1 - BITMAP_VRAM_0); //get rid of main screen offset
	rendering_flipBuffers();
}
