/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __MACROS
#define __MACROS

#include <stdint.h>
#include <cx16.h>

#define SET_CTRL(dcsel, addrsel) VERA.control = (dcsel<<1) | addrsel
#define SET_RAM_BANK(value) RAM_BANK = value

extern void utils_sleep(uint16_t time);
extern uint8_t rand3();
extern void utils_shuffleList(uint8_t * list, uint8_t lenFac); //length of list = 1<<lenFac
extern void debug_puts(char *s);
extern void debug_putc(char c);
extern void utils_resetMachine();

#endif
