/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __PLAYER
#define __PLAYER

#include <stdint.h>

//
#define PLAYER_UPDATE_NONE 0
#define PLAYER_UPDATE_NO_RENDER 1 //rendering isn't required
#define PLAYER_UPDATE_RENDER 2 //rendering is required
#define PLAYER_UPDATE_MOVE 3 //the world moves one tick

#define PLAYER_MAX_HP 20
#define PLAYER_MAX_HP_EASY 25


extern uint8_t player_x;
extern uint8_t player_y;
extern uint8_t player_rotation;

extern uint8_t player_isEasyMode;
extern uint8_t player_progress;

extern uint8_t player_hasInteracted;
extern uint8_t player_interactX;
extern uint8_t player_interactY;

extern uint8_t player_baseStrength;
extern uint8_t player_baseMagic;
extern uint8_t player_baseArmor;

extern uint8_t player_itemStrength;
extern uint8_t player_itemMagic;
extern uint8_t player_itemArmor;

extern uint8_t player_strength;
extern uint8_t player_magic;
extern uint8_t player_armor;

extern uint8_t player_gold;
extern uint8_t player_maxHp;
extern uint8_t player_hp;

extern void player_init();
extern uint8_t player_update(); //return 1 if player pressed a button but didn't move, 2 if he moved, otherwise 0
extern void player_setEasyMode(uint8_t value);
extern void player_damage(uint8_t dmg);
extern void player_updateHud();
extern void player_onInteract();
#endif
