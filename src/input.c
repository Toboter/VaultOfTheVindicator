/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "input.h"

#include <cx16.h>
#include <stdint.h>

uint8_t keyDown[128]; //0 if not pressed, 1 if pressed

uint8_t kscancodes[12] = {0x12, 0x1f, 0x20, 0x21, 0x25, 0x18, 0x27, 0x26, 0x11, 0x13, 0x23, 0x24}; //wasd ijkl qe gh
uint8_t alt_kscancodes[12] = {0x53, 0x4f, 0x54, 0x59, 0x25, 0x18, 0x27, 0x26, 0x17, 0x19, 0x23, 0x24}; //cursors ijkl uo gh


uint8_t input_inputsHeld[12];
uint8_t input_inputsPressed[12];


//written in assembly, cause it just doesn't work in C
volatile uint8_t keyin;
void keyhandler() {
	asm volatile ("tax");
	asm volatile ("and #$7f");
	asm volatile ("tay");
	asm volatile ("txa");
	asm volatile ("and #$80");
	asm volatile ("sta %v, y", keyDown);
	asm volatile ("lda #$00");
	return;
}


void input_init() {
	uint8_t i=0;
	for (i=0;i<128;i++) {
		keyDown[i] = 1;
	}

	for (i=0;i<12;i++) {
		input_inputsHeld[i] = 0;
		input_inputsPressed[i] = 0;
	}

	*((void(**)(void)) 0x32e) = keyhandler;
}

uint8_t input_isKeyDown(uint8_t keycode) {
	return !keyDown[keycode];
}



uint8_t joya,joyx,joyy;
uint8_t joyState[12] = {1,1,1,1,1,1,1,1,1,1,1,1};

void input_update(uint8_t invalidateReleased) {
	uint8_t i, newState;

	//poll joypad
	asm volatile("lda #$01");
	asm volatile("jsr $FF56"); //joystick_get(1)
	asm volatile("sta %v", joya);
	asm volatile("stx %v", joyx);
	asm volatile("sty %v", joyy);

	if (joyy == 0) {
		joyState[X_INPUT_UP] = joya & (1<<3);
		joyState[X_INPUT_LEFT] = joya & (1<<1);
		joyState[X_INPUT_DOWN] = joya & (1<<2);
		joyState[X_INPUT_RIGHT] = joya & (1<<0);
		joyState[X_INPUT_Y] = joya & (1<<6);
		joyState[X_INPUT_X] = joyx & (1<<6);
		joyState[X_INPUT_A] = joyx & (1<<7);
		joyState[X_INPUT_B] = joya & (1<<7);
		joyState[X_INPUT_L] = joyx & (1<<5);
		joyState[X_INPUT_R] = joyx & (1<<4);
		joyState[X_INPUT_SELECT] = joya & (1<<5);
		joyState[X_INPUT_START] = joya & (1<<4);
	} else {
		for (i=0;i<12;i++) joyState[i] = 1;
	}

	for (i=0;i<12;i++) {
		newState = (!keyDown[kscancodes[i]] || !keyDown[alt_kscancodes[i]] || !joyState[i]);

		if (newState && !input_inputsHeld[i]) input_inputsPressed[i] = 1;
		else if (invalidateReleased) input_inputsPressed[i] = 0;

		input_inputsHeld[i] = newState;
	}


}
