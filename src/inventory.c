/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "inventory.h"

#include "hud.h"
#include "items.h"
#include "utils.h"



#define INVENTORY_LENGTH 6

uint8_t inventory[INVENTORY_LENGTH];
uint8_t equippedArmor = INVENTORY_NO_EQUIP;
uint8_t cursorPos = 0;

//return 1 on success, 0 if inventory is full
uint8_t inventory_addItem(uint8_t item) {
	uint8_t i=0;
	for (;i<INVENTORY_LENGTH;i++) {
		if (!inventory[i]) {
			inventory[i] = item;
			return 1;
		}
	}
	return 0;
}

void   inventory_setEquippedArmor(uint8_t value) {
	equippedArmor = value;

}

uint8_t inventory_getCursorPos() {
	return cursorPos;
}

uint8_t   inventory_getItem(uint8_t slot) {
	return inventory[slot];
}

uint8_t inventory_getSelectedItem() {
	return inventory[cursorPos];
}

uint8_t   inventory_popItem(uint8_t slot) {
	uint8_t ret = inventory[slot];
	if (slot == equippedArmor) inventory_setEquippedArmor(INVENTORY_NO_EQUIP);
	inventory[slot] = 0;
	return ret;
}

uint8_t inventory_popCurrentItem() {
	return inventory_popItem(cursorPos);
}

void inventory_updateHUD() {
	uint8_t i=0;
	for (;i<INVENTORY_LENGTH;i++) {
		hud_setInventorySlot(i, inventory[i]);
	}

	hud_setInventoryCursor(HUD_INV_CURSOR1, cursorPos);
	hud_setInventoryCursor(HUD_INV_CURSOR2, equippedArmor);
}

//return 0 if none equipped
uint8_t inventory_getEquippedArmor() {
	if (equippedArmor == INVENTORY_NO_EQUIP) return 0;
	return inventory[equippedArmor];
}

void inventory_cursorUp() {
	--cursorPos;
	if (cursorPos == 255) cursorPos = INVENTORY_LENGTH-1;
}

void inventory_cursorDown() {
	++cursorPos;
	if (cursorPos >= INVENTORY_LENGTH) cursorPos = 0;
}

void inventory_init() {
	uint8_t i=0;
	cursorPos = 0;
	inventory_setEquippedArmor(INVENTORY_NO_EQUIP);
	for (;i<INVENTORY_LENGTH;i++) {
		inventory_popItem(i);
	}
}
