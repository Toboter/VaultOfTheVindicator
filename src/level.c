/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "level.h"

//#define __CX16__
#include <cbm.h>

#include "utils.h"
#include "rendering.h"
#include "hud.h"
#include "items.h"
#include "monsters.h"
#include "objects.h"
#include "player.h"
#include "sound.h"

#define MINIMAP_UNMARKED 0x42
#define MINIMAP_MARKED 0x10


Level * const level_loadedLevel = (Level*const)0xa000;
uint8_t level_currentLevel = 0;
uint8_t level_currentLevelBank = FIRST_LEVEL_BANK; //contains the ram bank of the currently loaded level
uint8_t level_currentObjectInteracted = 0;

//object rotation
int8_t xRotationOffset[4] = {0, -1, 0, 1};
int8_t yRotationOffset[4] = {1, 0, -1, 0};


void level_initHiram() {
	SET_RAM_BANK(1);
	cbm_load("hiram.bin", 8, (void*)0xa000);
}

void level_initCurrentLevel() {
	uint8_t i=0,j=0;

	for (i=0;i<16;i++)  {
		for (j=0;j<16;j++) {
			if (i<12 && j<12) {
				level_loadedLevel->minimap[i][j] = MINIMAP_UNMARKED;
			} else {
				level_loadedLevel->minimap[i][j] = 0;
			}
		}
	}

	for (i=0;i<LEVEL_SIZE;i++)  {
		for (j=0;j<LEVEL_SIZE;j++) {
			level_loadedLevel->collisionMap[i][j] = COLLISION_NONE;
		}
	}

	for (i=0; i<OBJECT_COUNT; i++) {
		level_loadedLevel->objects[i]._exists = 0;
	}

	level_loadedLevel->music = SOUND_MUSIC_NONE;
}

void level_updateMinimap(uint8_t x, uint8_t y) {
	uint8_t i=0,j=0;
	level_loadedLevel->minimap[y>>1][x>>1] = MINIMAP_MARKED;

	VERA.address_hi = 0b00010001;
	VERA.address = 0x5000 + (26*256);
	for (;i<16;i++)  {
		for (j=0;j<16;j++) {
			VERA.data0 = level_loadedLevel->minimap[i][j];
		}
	}
}

void level_changeLevelFast(uint8_t nextLevel) {
	level_currentLevel = nextLevel;
	level_currentLevelBank = FIRST_LEVEL_BANK + nextLevel;
	SET_RAM_BANK(level_currentLevelBank);
}

void level_changeLevel(uint8_t nextLevel, uint8_t isEntrance) {
	level_changeLevelFast(nextLevel);

	rendering_ceilingColor = level_loadedLevel->ceilingColor;
	rendering_distanceColor = level_loadedLevel->distanceColor;
	rendering_floorColor = level_loadedLevel->floorColor;

	switch (isEntrance) {
		case 0:
			player_x = level_loadedLevel->exitX;
			player_y = level_loadedLevel->exitY;
			player_rotation = WALL_LEFT;
			break;
		case 1:
			player_x = level_loadedLevel->entranceX;
			player_y = level_loadedLevel->entranceY;
			player_rotation = WALL_RIGHT;
			break;
		case 2:
			player_x = level_loadedLevel->specialX;
			player_y = level_loadedLevel->specialY;
			player_rotation = WALL_DOWN;
			break;
	}

	rendering_cameraX = player_x;
	rendering_cameraY = player_y;
	rendering_cameraRotation = player_rotation;

	sound_anticipateMusic(level_loadedLevel->music);

	rendering_resetBuffers();
}

Object *  level_allocNewObject(uint8_t x, uint8_t y, uint8_t objectType) {
	uint8_t i=0;
	Object * obj;
	SET_RAM_BANK(level_currentLevelBank);
	if (level_loadedLevel->collisionMap[x][y]) return NULL;

	obj = level_loadedLevel->objects;
	for (; i<OBJECT_COUNT; i++, obj++) {
		if (!(obj->_exists)) {
			obj->_exists = 1;
			obj->_objectType = objectType;
			level_moveObject(obj, x, y);
			return obj;
		}
	}
	return NULL;
}


void  level_deleteObject(Object * object) {
	object->_exists = 0;
	SET_RAM_BANK(level_currentLevelBank);
	if (object->_objectType & OBJECT_TYPE_HAS_COLLISION) {
		level_loadedLevel->collisionMap[object->_x][object->_y] = COLLISION_NONE;
	}
}


uint8_t  level_moveObject(Object * object, uint8_t x, uint8_t y) {
	SET_RAM_BANK(level_currentLevelBank);
	if (level_loadedLevel->collisionMap[x][y]) return 0;

	if (object->_objectType & OBJECT_TYPE_HAS_COLLISION) {
		level_loadedLevel->collisionMap[object->_x][object->_y] = COLLISION_NONE;
		level_loadedLevel->collisionMap[x][y] = COLLISION_OBJECT;
	}

	object->_x = x;
	object->_y = y;
	return 1;
}

void level_updateObjects() {
	uint8_t i, currLvl;
	Object * obj;
	SET_RAM_BANK(level_currentLevelBank);
	obj = level_loadedLevel->objects;
	currLvl = level_currentLevel;
	for (i=0; i<OBJECT_COUNT && currLvl == level_currentLevel; i++, obj++) {
		if (obj->_exists) {
			level_currentObjectInteracted = player_hasInteracted && player_interactX == obj->_x && player_interactY == obj->_y;
			switch (obj->_objectType & OBJECT_TYPE_ID) {
				case OBJECT_ID_DECORATION:
					break;
				case OBJECT_ID_ITEM:
					items_updateItemObject(obj);
					break;
				case OBJECT_ID_MONSTER:
					monsters_updateMonster(obj);
					break;
				case OBJECT_ID_MONEY_BAG:
					objects_updateMoneyBag(obj);
					break;
				case OBJECT_ID_CRAFTING_TABLE:
					object_updateCraftingTable(obj);
					break;
				case OBJECT_ID_VENDOR:
					objects_updateVendor(obj);
					break;
				case OBJECT_ID_UP_DOOR:
					object_updateUpDoor();
					break;
				case OBJECT_ID_DOWN_DOOR:
					object_updateDownDoor();
					break;
				case OBJECT_ID_PORTAL_DOOR:
					object_updatePortalDoor(obj);
					break;
				case OBJECT_ID_GUARD:
					object_updateGuard(obj);
					break;
			}
		}
	}
}

void level_doObjectWalls(uint8_t remove) {
	uint8_t i=0, x, y, wall;
	int8_t offX, offY;
	Object * obj;
	WALL_ORIENTATION wallDir;

	SET_RAM_BANK(level_currentLevelBank);

	obj = level_loadedLevel->objects;
	offX = xRotationOffset[rendering_cameraRotation];
	offY = yRotationOffset[rendering_cameraRotation];

	wallDir = rendering_cameraRotation;

	for (i=0; i<OBJECT_COUNT; i++, obj++) {
		if (obj->_exists && obj->wallType) {
			x = obj->_x;
			y = obj->_y;

			x += offX;
			y += offY;

			if (x>31 || y > 31) continue;

			wall = level_loadedLevel->wallData[x][y][wallDir];

			if (remove) {
				if (wall & WALL_FLAG_OBJECT) {
					level_loadedLevel->wallData[x][y][wallDir] = 0;
				}
			} else {
				if (wall == 0 || ((obj->_objectType & OBJECT_TYPE_VISUAL_PRIORITY) && (wall & WALL_FLAG_OBJECT))) {
					level_loadedLevel->wallData[x][y][wallDir] = obj->wallType;
				}
			}
		}
	}
}

uint8_t  level_getWall(uint8_t x, uint8_t y, WALL_ORIENTATION orientation) {
	SET_RAM_BANK(level_currentLevelBank);
	return level_loadedLevel->wallData[x][y][orientation];
}

void  level_setWall(uint8_t x, uint8_t y, WALL_ORIENTATION orientation, uint8_t wall) {
	SET_RAM_BANK(level_currentLevelBank);
	level_loadedLevel->wallData[x][y][orientation] = wall;
}
