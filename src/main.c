/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "game.h"
#include "input.h"
#include "rendering.h"
#include "sound.h"
#include "hud.h"
#include "utils.h"
#include "player.h"

#include <cx16.h>

int main() {
	sound_init();
	input_init();
	rendering_initRendering();

	player_setEasyMode(0);

	game_changeMode(GAME_MODE_TITLE);

	game_run();

	return 0;
}
