/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __ITEMS
#define __ITEMS

#include <stdint.h>
#include "level.h"

#define ITEM_GET_TYPE(item_id) (item_id-1)>>2

#define ITEM_TYPE_SWORD 0
#define ITEM_TYPE_ARMOR 1
#define ITEM_TYPE_CRAFTABLE 2
#define ITEM_TYPE_SCROLL 3

#define ITEM_SWORD_1 1
#define ITEM_SWORD_2 2
#define ITEM_SWORD_3 3
#define ITEM_SWORD_4 4
#define ITEM_ARMOR_1 5
#define ITEM_ARMOR_2 6
#define ITEM_ARMOR_3 7
#define ITEM_ARMOR_4 8
#define ITEM_CRAFT_CRYSTAL 9
#define ITEM_CRAFT_PLANT 10
#define ITEM_CRAFT_BONE 11
#define ITEM_CRAFT_SPECIAL 12
#define ITEM_SCROLL_1 13
#define ITEM_SCROLL_2 14
#define ITEM_SCROLL_3 15
#define ITEM_SCROLL_4 16
#define ITEM_MISC_POTION 17
#define ITEM_MISC_WAND 18
#define ITEM_MISC_PORTAL_KEY 19
#define ITEM_MISC_GOLD_KEY 20
#define ITEM_MISC_GHOST_KEY 21

extern char * items_itemNames[];

extern void items_randomizeScrolls();

extern uint8_t items_spawnItemObject(uint8_t x, uint8_t y, uint8_t itemID);

extern void items_updateItemObject(Object * object);

extern void items_applyHeldEffects(uint8_t heldItemID, uint8_t equippedItemID);

extern uint8_t items_useItem(uint8_t itemID);

#endif
