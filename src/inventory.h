/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __INVENTORY
#define __INVENTORY

#include <stdint.h>

#define INVENTORY_NO_EQUIP 10 //move armor cursor far off screen

extern uint8_t inventory_addItem(uint8_t item); //return 1 on success, 0 if inventory is full
extern void inventory_setEquippedArmor(uint8_t value);
extern uint8_t inventory_getCursorPos();
extern uint8_t inventory_getItem(uint8_t slot);
extern uint8_t inventory_popItem(uint8_t slot);
extern uint8_t inventory_popCurrentItem();
extern uint8_t inventory_getSelectedItem();
extern uint8_t inventory_getEquippedArmor(); //0 if none equipped
extern void inventory_updateHUD();
extern void inventory_cursorUp();
extern void inventory_cursorDown();
extern void inventory_init();

#endif
