/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "player.h"

#include <stdlib.h>

#include "input.h"
#include "level.h"
#include "rendering.h"
#include "inventory.h"
#include "items.h"
#include "hud.h"
#include "utils.h"
#include "game.h"
#include "sound.h"



uint8_t player_x = 8;
uint8_t player_y = 8;
uint8_t player_rotation = 2;

uint8_t player_hasInteracted = 0;
uint8_t player_interactX;
uint8_t player_interactY;

uint8_t player_progress = 0;

uint8_t player_baseStrength = 0;
uint8_t player_baseMagic = 0;
uint8_t player_baseArmor = 0;

uint8_t player_itemStrength;
uint8_t player_itemMagic;
uint8_t player_itemArmor;

uint8_t player_strength;
uint8_t player_magic;
uint8_t player_armor;

uint8_t player_gold;
uint8_t player_maxHp;
uint8_t player_hp;

uint8_t player_isEasyMode;

void player_init() {
	rendering_cameraX = player_x;
	rendering_cameraY = player_y;
	rendering_cameraRotation = player_rotation;

	player_hasInteracted = 0;
	player_baseStrength = 0;
	player_baseMagic = 0;
	player_baseArmor = 0;
	player_strength = 0;
	player_magic = 0;
	player_armor = 0;
	player_gold = 0;
	player_progress = 0;

	player_hp = player_maxHp;
}

void player_setEasyMode(uint8_t value) {
	player_isEasyMode = value;
	player_maxHp = value ? PLAYER_MAX_HP_EASY : PLAYER_MAX_HP;
}


int8_t xMoveByButtonDir[4][4] = {
	{0,1,0,-1},
	{-1, 0, 1, 0},
	{0, -1, 0, 1},
	{1, 0, -1, 0}
};

int8_t yMoveByButtonDir[4][4] = {
	{-1,0,1,0},
	{0, -1, 0, 1},
	{1, 0, -1, 0},
	{0, 1, 0, -1}
};

void player_damage(uint8_t dmg) {

	if ((dmg <= player_armor) && (rand()&1)) dmg = player_armor+1;

	if (dmg > player_armor) {
		 hud_displayEffect(HUD_EFFECT_BLOOD);
		 dmg -= player_armor;

		 sound_playSFX(SOUND_EFFECT_HIT, SOUND_PRIORITY_HIT);

		 if (dmg >= player_hp) {
			 player_hp = 0;
			 player_updateHud();
			 hud_clearText();
			 hud_print("You are dead");
			 sound_playMusic(SOUND_MUSIC_GAME_OVER);
			 utils_sleep(450);
			 hud_hideAllEffects();
			 game_changeMode(GAME_MODE_GAME_OVER);
		 }

		 player_hp -= dmg;
	}
}

void player_updateHud() {
	hud_setDisplayValue(HUD_VALUE_HP, player_hp);
	hud_setDisplayValue(HUD_VALUE_GOLD, player_gold);
	hud_setDisplayValue(HUD_VALUE_STR, player_strength);
	hud_setDisplayValue(HUD_VALUE_MAG, player_magic);
	hud_setDisplayValue(HUD_VALUE_DEF, player_armor);
}

void player_onInteract() {
	sound_stopChannel(SOUND_PRIORITY_ACTION);
	hud_hideEffect(HUD_EFFECT_SWORD);
	hud_hideEffect(HUD_EFFECT_MAGIC);
}


uint8_t player_update() {
	int8_t moveX=0,moveY=0;
	uint8_t ret=PLAYER_UPDATE_NONE;

	player_hasInteracted = 0;


	//Turning
	if (input_inputsPressed[X_INPUT_L]) {
		player_rotation = (player_rotation-1)&0b11;
		ret = PLAYER_UPDATE_RENDER;
	}
	else if (input_inputsPressed[X_INPUT_R]) {
		player_rotation = (player_rotation+1)&0b11;
		ret = PLAYER_UPDATE_RENDER;
	}

	//Moving
	else if (input_inputsHeld[X_INPUT_LEFT]) {
		moveX = xMoveByButtonDir[1][player_rotation];
		moveY = yMoveByButtonDir[1][player_rotation];
		ret = PLAYER_UPDATE_MOVE;
	}
	else if (input_inputsHeld[X_INPUT_RIGHT]) {
		moveX = xMoveByButtonDir[3][player_rotation];
		moveY = yMoveByButtonDir[3][player_rotation];
		ret = PLAYER_UPDATE_MOVE;
	}
	else if (input_inputsHeld[X_INPUT_UP]) {
		moveX = xMoveByButtonDir[0][player_rotation];
		moveY = yMoveByButtonDir[0][player_rotation];
		ret = PLAYER_UPDATE_MOVE;
	}
	else if (input_inputsHeld[X_INPUT_DOWN]) {
		moveX = xMoveByButtonDir[2][player_rotation];
		moveY = yMoveByButtonDir[2][player_rotation];
		ret = PLAYER_UPDATE_MOVE;
	}

	//pause
	else if (input_inputsHeld[X_INPUT_START]) {
		game_changeMode(GAME_MODE_PAUSED);
	}

	//Inventory scrolling
	else if (input_inputsPressed[X_INPUT_X]) {
		inventory_cursorUp();
		ret = PLAYER_UPDATE_NONE;
	}
	else if (input_inputsPressed[X_INPUT_B]) {
		inventory_cursorDown();
		ret = PLAYER_UPDATE_NONE;
	}

	//Wait
	else if (input_inputsPressed[X_INPUT_SELECT]) {
		sound_playSFX(SOUND_EFFECT_BOOP, SOUND_PRIORITY_ACTION);
		hud_print("Waiting...");
		ret = PLAYER_UPDATE_MOVE;
	}

	//using items
	else if (input_inputsHeld[X_INPUT_Y]) {
		player_interactX = player_x + xMoveByButtonDir[0][player_rotation];
		player_interactY = player_y + yMoveByButtonDir[0][player_rotation];

		if (!items_useItem(inventory_getSelectedItem())) {
			player_hasInteracted = 1;
		}

		ret = PLAYER_UPDATE_MOVE;
	}

	//dropping items
	else if (input_inputsPressed[X_INPUT_A]) {
		if (inventory_getSelectedItem() && !(level_getWall(player_x, player_y, player_rotation) & WALL_FLAG_TYPE)) {
			if (items_spawnItemObject(
				player_x + xMoveByButtonDir[0][player_rotation],
				player_y + yMoveByButtonDir[0][player_rotation],
				inventory_getSelectedItem())) {
					sound_playSFX(SOUND_EFFECT_BOOP, SOUND_PRIORITY_ACTION);
					inventory_popCurrentItem();
				}

			ret = PLAYER_UPDATE_MOVE;
		}
	}

	//collision and movement
	if (moveX == 1 && (level_getWall(player_x, player_y, WALL_RIGHT) & WALL_FLAG_TYPE)) moveX = 0;
	if (moveX == -1 && (level_getWall(player_x, player_y, WALL_LEFT) & WALL_FLAG_TYPE)) moveX = 0;
	if (moveY == 1 && (level_getWall(player_x, player_y, WALL_DOWN) & WALL_FLAG_TYPE)) moveY = 0;
	if (moveY == -1 && (level_getWall(player_x, player_y, WALL_UP) & WALL_FLAG_TYPE)) moveY = 0;

	moveX = player_x + moveX;
	moveY = player_y + moveY;
	if (!level_loadedLevel->collisionMap[moveX][moveY]) {
		player_x = moveX;
		player_y = moveY;
	}


	//recalculate stats
	items_applyHeldEffects(inventory_getSelectedItem(), inventory_getEquippedArmor());
	player_strength = player_itemStrength + player_baseStrength;
	player_magic = player_baseMagic + player_itemMagic;
	player_armor = player_baseArmor + player_itemArmor;

	rendering_cameraX = player_x;
	rendering_cameraY = player_y;
	rendering_cameraRotation = player_rotation;

	return ret;
}
