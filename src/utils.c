/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "utils.h"

#include <cbm.h>
#include <stdlib.h>
#include <stdio.h>

#include "input.h"


uint8_t timeLow, timeHigh;
uint16_t rdtim() {
	asm volatile("jsr $ffde"); //rdtim
	asm volatile("sta %v", timeLow);
	asm volatile("stx %v", timeHigh);
	return (timeHigh<<8) | timeLow;
}

void utils_sleep(uint16_t time) {
	//return;
	cbm_k_settim(0);
	while(rdtim() < time) {}
}

uint8_t rand3() {
	uint8_t ret = 3;
	while(ret == 3) {
		ret = rand() & 0b11;
	}
	return ret;
}

//length of list = 1<<lenFac
void utils_shuffleList(uint8_t * list, uint8_t lenFac) {
	uint8_t randMask, len, tmp, j;
	uint8_t i = 0;
	len = 1<<lenFac;
	randMask = len-1;
	for (;i<len;i++) {
		j = rand()&randMask;
		tmp = list[i];
		list[i] = list[j];
		list[j] = tmp;
	}
}


void utils_resetMachine() {
	asm volatile("ldx #$42"); //write 1 to SMC reg 1 for reboot
	asm volatile("ldy #$02");
	asm volatile("lda #$00");
	asm volatile("jsr $FEC9");
}


#define DEBUG_PORT 0x9fbb

void debug_putc(char ch) {
  *(char *)DEBUG_PORT = ch;
  if (ch == 13)
    *(char *)DEBUG_PORT = 10;
}

void debug_puts(char *s) {
  while (*s)
    debug_putc(*s++);
}
