/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "game.h"

#include "level.h"
#include "rendering.h"
#include "input.h"
#include "player.h"
#include "hud.h"
#include "inventory.h"
#include "mapgen.h"
#include "items.h"
#include "monsters.h"
#include "objects.h"
#include "sound.h"


GAME_MODE currentMode = 0xff;

void game_changeMode(GAME_MODE mode) {
	currentMode = mode;

	level_initHiram();
	rendering_resetBuffers();
	hud_init();
	input_update(1);


	switch (mode) {
		case GAME_MODE_TITLE:
			sound_loadMusic(SOUND_MUSIC_TITLE);
			rendering_renderScreen("title.scr");
			sound_playMusic(SOUND_MUSIC_TITLE);
			break;

		case GAME_MODE_PAUSED:
			rendering_renderScreen("pause.scr");
			break;

		case GAME_MODE_END1:
			sound_loadMusic(SOUND_MUSIC_END);
			rendering_renderScreen("end1.scr");
			sound_playMusic(SOUND_MUSIC_END);
			break;

		case GAME_MODE_END2:
			sound_loadMusic(SOUND_MUSIC_END);
			rendering_renderScreen("end2.scr");
			sound_playMusic(SOUND_MUSIC_END);
			break;

		case GAME_MODE_END3:
			sound_loadMusic(SOUND_MUSIC_END);
			rendering_renderScreen("end3.scr");
			sound_playMusic(SOUND_MUSIC_END);
			break;

		case GAME_MODE_END4:
			sound_loadMusic(SOUND_MUSIC_END);
			rendering_renderScreen("end4.scr");
			sound_playMusic(SOUND_MUSIC_END);
			break;

		case GAME_MODE_GAME_OVER:
			sound_anticipateMusic(SOUND_MUSIC_GAME_OVER_LOOP);
			rendering_renderScreen("gameover.scr");
			utils_sleep(60);
			sound_playMusic(SOUND_MUSIC_GAME_OVER_LOOP);
			break;

		case GAME_MODE_NEW_GAME:
			sound_playMusic(SOUND_MUSIC_NONE);
			break;

		case GAME_MODE_PLAY:
			hud_setEnabled(1);
			SET_RAM_BANK(level_currentLevelBank);
			sound_playMusic(level_loadedLevel->music);

			player_updateHud();
			inventory_updateHUD();
			hud_setMapCursor(player_x>>1, player_y>>1);
			hud_setCompassCursor(player_rotation);
			//level_updateMinimap(0, 0);
			level_updateMinimap(player_x, player_y);

			level_doObjectWalls(0);
			rendering_renderBitmap();
			level_doObjectWalls(1);
			break;
	}
}


void initializeGame() {
	inventory_init();
	player_init();
	player_updateHud();
	inventory_updateHUD();

	hud_setEnabled(1);
	hud_print("Descend\nFind him");
	sound_playMusic(SOUND_MUSIC_DESCEND);
	//sound_playSFX(SOUND_EFFECT_STAIRS, SOUND_PRIORITY_ACTION);

	mapgen_init();
	mapgen_generateWorld();
	items_randomizeScrolls();



	inventory_addItem(ITEM_SWORD_1);
	inventory_addItem(ITEM_ARMOR_1);
	/*inventory_addItem(ITEM_SWORD_4);
	inventory_addItem(ITEM_ARMOR_4);
	inventory_addItem(ITEM_MISC_WAND);
	inventory_addItem(ITEM_SCROLL_1);
	level_changeLevel(10, 2);*/

	inventory_setEquippedArmor(1);
	utils_sleep(180);
	level_changeLevel(0, 1);
	player_update();

	hud_clearText();
	objects_printLevelHeader();
	utils_sleep(60);
	game_changeMode(GAME_MODE_PLAY);
}

void updateGame() {
	uint8_t playerUpdate;
	input_update(1);
	hud_clearText();

	playerUpdate = player_update();

	if (input_isKeyDown(0x70)) game_changeMode(GAME_MODE_PAUSED); //F1 opens pause

	if (currentMode != GAME_MODE_PLAY) return;

	if (playerUpdate >= PLAYER_UPDATE_RENDER) {
		if (playerUpdate >= PLAYER_UPDATE_MOVE) level_updateObjects();

		if (currentMode != GAME_MODE_PLAY) return;

		level_doObjectWalls(0);
		rendering_renderBitmap();
		level_doObjectWalls(1);

	}

	hud_setMapCursor(player_x>>1, player_y>>1);
	hud_setCompassCursor(player_rotation);
	inventory_updateHUD();
	player_updateHud();
	level_updateMinimap(player_x, player_y);

	hud_hideAllEffects();

	if (playerUpdate >= PLAYER_UPDATE_NO_RENDER) utils_sleep(16);


}

void updateTitleScreen() {
	input_update(1);

	if (input_inputsPressed[X_INPUT_START]) {
		player_setEasyMode(0);
		game_changeMode(GAME_MODE_NEW_GAME);
	}
	if (input_inputsPressed[X_INPUT_SELECT]) {
		player_setEasyMode(1);
		game_changeMode(GAME_MODE_NEW_GAME);
	}

	if (input_isKeyDown(0x6e)) {
		utils_resetMachine();
	}
}

void updatePauseScreen() {
	input_update(1);

	if (input_inputsPressed[X_INPUT_START]) {
		game_changeMode(GAME_MODE_PLAY);
	}
	if (input_inputsPressed[X_INPUT_SELECT]) {
		game_changeMode(GAME_MODE_TITLE);
	}
}

void updateEndScreen() {
	input_update(1);

	if (input_inputsPressed[X_INPUT_START]) {
		game_changeMode(GAME_MODE_TITLE);
	}
}


void game_run() {
	while (1) {
		switch (currentMode) {
			case GAME_MODE_TITLE:
				updateTitleScreen();
				break;

			case GAME_MODE_PAUSED:
				updatePauseScreen();
				break;
			case GAME_MODE_NEW_GAME:
				initializeGame();
				break;

			case GAME_MODE_PLAY:
				updateGame();
				break;

			case GAME_MODE_END1:
			case GAME_MODE_END2:
			case GAME_MODE_END3:
			case GAME_MODE_END4:
			case GAME_MODE_GAME_OVER:
				updateEndScreen();
				break;
		}
	}
}
