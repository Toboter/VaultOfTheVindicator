/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __RENDERING
#define __RENDERING

#include <stdint.h>

extern uint8_t rendering_cameraX;
extern uint8_t rendering_cameraY;
extern uint8_t rendering_cameraRotation;

extern uint8_t rendering_ceilingColor;
extern uint8_t rendering_distanceColor;
extern uint8_t rendering_floorColor;

extern void rendering_initRendering();
extern void rendering_renderBitmap();
extern void rendering_flipBuffers();
extern void rendering_resetBuffers();//call before rendering again after changing colors

extern void rendering_renderScreen(char * screenName);

#endif
