/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "monsters.h"

//#define __CX16__
#include <stdlib.h>

#include "level.h"
#include "player.h"
#include "inventory.h"
#include "items.h"
#include "hud.h"
#include "objects.h"
#include "game.h"
#include "sound.h"


uint8_t monsterWalls[] = {14,15,16,17,18, 27};
uint8_t corpseWalls[] = {23, 24, 25, 26, 26, 26};
uint8_t monsterHp[] = {3, 4, 5, 10, 20, 127};
uint8_t monsterAttack[] = {2, 3, 4, 4, 5, 1};
uint8_t monsterDefense[] = {0, 1, 1, 2, 2, 10};

#define MONSTER_STATE_IDLE 0
#define MONSTER_STATE_ATTACKING 1

#define MONSTER_DETECT_PLAYER_RANGE 6
#define MONSTER_LEAVE_PLAYER_RANGE 8

void monsters_spawnMonster(uint8_t x, uint8_t y, uint8_t monsterType) {
	Object * mon = level_allocNewObject(x, y,  OBJECT_ID_MONSTER | OBJECT_TYPE_HAS_COLLISION |OBJECT_TYPE_VISUAL_PRIORITY);
	if (mon) {
		mon->wallType = monsterWalls[monsterType] | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
		mon->data[MONSTER_DATA_TYPE] = monsterType;
		mon->data[MONSTER_DATA_HP] = monsterHp[monsterType];
		mon->data[MONSTER_DATA_ATTACK] = monsterAttack[monsterType];
		mon->data[MONSTER_DATA_DEFENSE] = monsterDefense[monsterType];

		mon->data[MONSTER_DATA_STATE] = MONSTER_STATE_IDLE;
		mon->data[MONSTER_DATA_EFFECTS] = 0;
		mon->data[MONSTER_DATA_EFFECT_TIMER] = 0;
	}
}

void monsters_applyEffect(Object * mon, uint8_t effect, uint8_t duration) {
	mon->data[MONSTER_DATA_EFFECTS] |= effect;
	mon->data[MONSTER_DATA_EFFECT_TIMER] = duration;
}

void monsters_updateMonster(Object * mon) {
	uint8_t playerDistX, playerDistY, playerDist, selectedItem, dmg, monType ,x, y;

	x = mon->_x;
	y= mon->_y;
	//if attacked
	if (level_currentObjectInteracted) {
		player_onInteract();
		selectedItem = inventory_getSelectedItem();

		if (selectedItem) {
			dmg = 0;
			if (ITEM_GET_TYPE(selectedItem) == ITEM_TYPE_SWORD && player_strength > mon->data[MONSTER_DATA_DEFENSE]) {
				dmg = player_strength - mon->data[MONSTER_DATA_DEFENSE];
				sound_playSFX(SOUND_EFFECT_SWORD_HIT, SOUND_PRIORITY_ACTION);
				hud_displayEffect(HUD_EFFECT_BLOODY_SWORD);

			} else if (selectedItem == ITEM_MISC_WAND) {
				dmg = player_magic;
				sound_playSFX(SOUND_EFFECT_MAGIC_ATTACK, SOUND_PRIORITY_ACTION);
				hud_displayEffect(HUD_EFFECT_BLOODY_MAGIC);
			}

			if (dmg == 0 && (rand() & 1)) {
				dmg = 1;
				sound_playSFX(SOUND_EFFECT_SWORD_HIT, SOUND_PRIORITY_ACTION);
				hud_displayEffect(HUD_EFFECT_BLOODY_SWORD);
			}

			mon->data[MONSTER_DATA_EFFECTS] &= ~MONSTER_EFFECT_CHARM;

			mon->data[MONSTER_DATA_HP] -= dmg;
		}
	}

	//determin distance from player
	if (x > player_x) {
		playerDistX = x - player_x;
	} else {
		playerDistX = player_x - x;
	}

	if (y > player_y) {
		playerDistY =y - player_y;
	} else {
		playerDistY = player_y - y;
	}

	playerDist = playerDistX + playerDistY;

	//main behaviour
	switch (mon->data[MONSTER_DATA_STATE]) {

		case MONSTER_STATE_IDLE:
			if (playerDist <= MONSTER_DETECT_PLAYER_RANGE) {
				mon->data[MONSTER_DATA_STATE] = MONSTER_STATE_ATTACKING;
			}
			break;

		case MONSTER_STATE_ATTACKING:
			if (playerDist > MONSTER_LEAVE_PLAYER_RANGE || (mon->data[MONSTER_DATA_EFFECTS] & MONSTER_EFFECT_CHARM) ) {
				mon->data[MONSTER_DATA_STATE] = MONSTER_STATE_IDLE;
				break;
			}

			if (playerDist <= 1) {
				player_damage(mon->data[MONSTER_DATA_ATTACK]);
			}

			else if (rand() & 0b11) {
				if (player_x > x && !level_getWall(x, y, WALL_RIGHT)) {
					if (level_moveObject(mon, x+1, y)) playerDist--;
				} else if (player_x < x && !level_getWall(x, y, WALL_LEFT)){
					if (level_moveObject(mon, x-1, y)) playerDist--;
				}

				if (playerDist > 1) { //ensure that the monster never moves onto the player
					if (player_y > y && !level_getWall(x, y, WALL_DOWN)) {
						level_moveObject(mon, x, y+1);
					} else if (player_y < y && !level_getWall(x, y, WALL_UP)){
						level_moveObject(mon, x, y-1);
					}
				}
			}
			break;
	}



	if (mon->data[MONSTER_DATA_EFFECTS] & MONSTER_EFFECT_POISON) {
		mon->data[MONSTER_DATA_HP]--;
	}

	if (mon->data[MONSTER_DATA_EFFECT_TIMER] > 0) {
		mon->data[MONSTER_DATA_EFFECT_TIMER]--;
	} else {
		mon->data[MONSTER_DATA_EFFECTS] = 0;
	}

	x = mon->_x;
	y= mon->_y;

	//if dead (check for overflow)
	if (mon->data[MONSTER_DATA_HP]-1 >= 128) {
		monType = mon->data[MONSTER_DATA_TYPE];
		level_deleteObject(mon);
		switch (monType) {
			case MONSTER_TYPE_ZOMBIE:
				if (level_currentLevel == 9) {
					items_spawnItemObject(x, y, ITEM_MISC_GOLD_KEY);
					break;
				}
			case MONSTER_TYPE_MAGE:
			case MONSTER_TYPE_DEMON:
				if (rand()&1) {
					objects_spawnMoneyBag(x,y);
				}
				break;

			case MONSTER_TYPE_DEMON_BOSS:
				items_spawnItemObject(x, y, ITEM_CRAFT_SPECIAL);
				break;

			case MONSTER_TYPE_NECRO_BOSS:
				game_changeMode(GAME_MODE_END2);
				break;
		}
		objects_spawnDecoration(x, y, corpseWalls[monType]);

	}
}
