/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "sound.h"

#include <cbm.h>

#include "level.h"
#include "utils.h"
#include "hud.h"

#define ZSM_BANK 3

#define SFX_BANK_1 4

#define MUSIC_BANK_START 18

uint8_t sfxAddressHigh[] = {0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6};

uint8_t currentMusic = SOUND_MUSIC_NONE;
uint8_t loadedMusic = SOUND_MUSIC_NONE;

char * musicNames[] = {
	"",
	"title.zsm",
	"area1.zsm",
	"area2.zsm",
	"area3.zsm",
	"area4.zsm",
	"area5.zsm",
	"dead.zsm",
	"deadloop.zsm",
	"descend.zsm",
	"end.zsm"
};

void loadSound(char* name, uint8_t index) {
	cbm_k_setlfs(0, 8, 2);
	cbm_k_setnam(name);
	cbm_k_load(0, ((uint16_t)sfxAddressHigh[index])<<8);
}


void sound_init() {
	uint8_t i=0;
	asm volatile ("lda #%b", ZSM_BANK);
	asm volatile ("jsr zsm_init_engine");
	asm volatile ("jsr zsmkit_setisr");


	SET_RAM_BANK(SFX_BANK_1);

	loadSound("hit.zsm", 0);
	loadSound("sword.zsm", 1);
	loadSound("swordhit.zsm", 2);
	loadSound("stairs.zsm", 3);
	loadSound("magiccast.zsm", 4);
	loadSound("magicattack.zsm", 5);
	loadSound("boop.zsm", 6);

	SET_RAM_BANK(level_currentLevelBank);
}

uint8_t sound_tmp, param1, param2;

void sound_playSFX(uint8_t effect, uint8_t priority) {
	SET_RAM_BANK(SFX_BANK_1);

	param1 = sfxAddressHigh[effect];
	param2 = priority;

	asm volatile ("ldx %v", param2);
	asm volatile ("jsr zsm_stop");

	asm volatile ("lda #$00");
	asm volatile ("ldx %v", param2);
	asm volatile ("ldy %v", param1); //address hi to Y
	asm volatile ("jsr zsm_setmem");


	asm volatile ("ldx %v", param2);
	asm volatile ("jsr zsm_play");
	SET_RAM_BANK(level_currentLevelBank);
}

void sound_stopChannel(uint8_t priority) {
	param2 = priority;
	asm volatile ("ldx %v", param2);
	asm volatile ("jsr zsm_stop");
}

void sound_anticipateMusic(uint8_t music) {
	if (music != currentMusic) {
		param2 = SOUND_PRIORITY_MUSIC;

		asm volatile ("ldx %v", param2);
		asm volatile ("jsr zsm_stop");
	}
}

void sound_loadMusic(uint8_t music) {
	param2 = SOUND_PRIORITY_MUSIC;

	asm volatile ("ldx %v", param2);
	asm volatile ("jsr zsm_stop");

	SET_RAM_BANK(MUSIC_BANK_START);
	cbm_k_setlfs(0, 8, 2);
	cbm_k_setnam(musicNames[music]);
	cbm_k_load(0, 0xa000);
	loadedMusic = music;
}

void sound_playMusic(uint8_t music) {
	if (music == currentMusic) return;

	currentMusic = music;

	param1 = 0xa0;
	param2 = SOUND_PRIORITY_MUSIC;

	asm volatile ("ldx %v", param2);
	asm volatile ("jsr zsm_stop");

	if (!music) return;

	if (loadedMusic != music) sound_loadMusic(music);

	SET_RAM_BANK(MUSIC_BANK_START);

	asm volatile ("lda #$00");
	asm volatile ("ldx %v", param2);
	asm volatile ("ldy %v", param1); //address hi to Y
	asm volatile ("jsr zsm_setmem");


	asm volatile ("ldx %v", param2);
	asm volatile ("jsr zsm_play");

	if (music == SOUND_MUSIC_GAME_OVER || music == SOUND_MUSIC_DESCEND) {
		asm volatile ("ldx %v", param2); //music loops not
		asm volatile ("clc");
		asm volatile ("jsr zsm_setloop");
	} else {
		asm volatile ("ldx %v", param2); //music loops
		asm volatile ("sec");
		asm volatile ("jsr zsm_setloop");
	}


	SET_RAM_BANK(level_currentLevelBank);
}
