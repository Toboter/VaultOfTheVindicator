/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __SOUND
#define __SOUND

#include <stdint.h>

#define SOUND_EFFECT_HIT 0
#define SOUND_EFFECT_SWORD 1
#define SOUND_EFFECT_SWORD_HIT 2
#define SOUND_EFFECT_STAIRS 3
#define SOUND_EFFECT_MAGIC_CAST 4
#define SOUND_EFFECT_MAGIC_ATTACK 5
#define SOUND_EFFECT_BOOP 6

#define SOUND_MUSIC_NONE 0
#define SOUND_MUSIC_TITLE 1
#define SOUND_MUSIC_AREA_1 2
#define SOUND_MUSIC_AREA_2 3
#define SOUND_MUSIC_AREA_3 4
#define SOUND_MUSIC_AREA_4 5
#define SOUND_MUSIC_AREA_5 6
#define SOUND_MUSIC_GAME_OVER 7
#define SOUND_MUSIC_GAME_OVER_LOOP 8
#define SOUND_MUSIC_DESCEND 9
#define SOUND_MUSIC_END 10

#define SOUND_PRIORITY_MUSIC 0
#define SOUND_PRIORITY_HIT 1
#define SOUND_PRIORITY_ACTION 2

extern void sound_init();
extern void sound_playSFX(uint8_t effect, uint8_t priority) ;
extern void sound_stopChannel(uint8_t priority);

extern void sound_loadMusic(uint8_t music);
extern void sound_playMusic(uint8_t music);
extern void sound_anticipateMusic(uint8_t music); //mutes current music if new music is different



#endif
