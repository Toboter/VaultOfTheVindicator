/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __HUD
#define __HUD

#include <stdint.h>

//2 each
#define HUD_VALUE_GOLD 45
#define HUD_VALUE_HP 47
#define HUD_VALUE_STR 49
#define HUD_VALUE_MAG 51
#define HUD_VALUE_DEF 53

#define HUD_INV_CURSOR1 55
#define HUD_INV_CURSOR2 56

#define HUD_EFFECT_BLOOD 76
#define HUD_EFFECT_SWORD 68
#define HUD_EFFECT_MAGIC 69
#define HUD_EFFECT_BLOODY_SWORD 75
#define HUD_EFFECT_BLOODY_MAGIC 67


extern void hud_init();
extern void hud_setEnabled(uint8_t enabled);

extern void hud_print(char * str);
extern void hud_printNumber(uint8_t num);
extern void hud_clearText();
extern void hud_setDisplayValue(uint8_t valueId, uint8_t value);
extern void hud_setInventorySlot(uint8_t slot, uint8_t value);
extern void hud_setInventoryCursor(uint8_t cursor, uint8_t slot);
extern void hud_setMapCursor(uint8_t x, uint8_t y);
extern void hud_setCompassCursor(uint8_t rotation);

extern void   hud_displayEffect(uint8_t effect);
extern void  hud_hideEffect(uint8_t effect);
extern void hud_hideAllEffects();

#endif
