/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "mapgen.h"

//#define __CX16__
#include <cx16.h>
#include <stdlib.h>
#include <string.h>

#include "level.h"
#include "utils.h"
#include "monsters.h"
#include "items.h"
#include "objects.h"
#include "hud.h"
#include "player.h"
#include "sound.h"

#define CORRIDOR_UP 0b01
#define CORRIDOR_RIGHT 0b10

#define DOOR_NONE 0
#define DOOR_DECORATIVE 1
#define DOOR_FULL 2

#define WALLSET_BASE_1 1
#define WALLSET_BASE_2 6
#define WALLSET_BASE_3 12
#define WALLSET_BASE_4 17
#define WALLSET_BASE_5 23

uint8_t corridors[4][4];

uint8_t mapOutline[32][32];


void mapgen_init() {
	__randomize();
}


uint8_t foundX, foundY; //output for find functions

void findObjectSpot() {
	foundX = 31;
	foundY = 31;
	while (mapOutline[foundX][foundY] != 1) {
		foundX = rand()&31;
		foundY = rand()&31;
	}
	mapOutline[foundX][foundY] = 2;
}

void findWall(WALL_ORIENTATION dir) {
	foundX = 31;
	foundY = 31;
	while (!level_getWall(foundX, foundY, dir)) {
		foundX = rand()&31;
		foundY = rand()&31;
		if (foundX == 0) foundX = 1;
		if (foundY == 0) foundY = 1;
	}
}

void placeMonster(uint8_t monsterID, uint8_t amount) {
	uint8_t i=0;
	for (;i<amount;i++){
		findObjectSpot();
		monsters_spawnMonster(foundX, foundY, monsterID);
	}
}

void placeItem(uint8_t itemID) {
	findObjectSpot();
	items_spawnItemObject(foundX, foundY, itemID);
}

void placeDoors(uint8_t levelBaseWall, uint8_t downDoorState, uint8_t upDoorState) {
	if (upDoorState) {
		findWall(WALL_LEFT);
		level_setWall(foundX, foundY, WALL_LEFT, levelBaseWall + 4);
		level_loadedLevel->entranceX = foundX;
		level_loadedLevel->entranceY = foundY;
		if (upDoorState == DOOR_FULL) objects_spawnUpDoorTrigger(foundX-1, foundY);
	}
	if (downDoorState) {
		findWall(WALL_RIGHT);
		level_setWall(foundX, foundY, WALL_RIGHT, levelBaseWall + 3);
		level_loadedLevel->exitX = foundX;
		level_loadedLevel->exitY = foundY;
		if (downDoorState == DOOR_FULL) objects_spawnDownDoorTrigger(foundX+1, foundY);
	}
}

void placePortal(uint8_t wall, uint8_t trigger, uint8_t targetLevel, uint8_t freeStanding) {
	if (freeStanding) {
		findObjectSpot();
		objects_spawnPortalDoorTrigger(foundX, foundY, wall, trigger, targetLevel);
	} else {
		findWall(WALL_UP);
		level_setWall(foundX, foundY, WALL_UP, wall);
		objects_spawnPortalDoorTrigger(foundX, foundY-1, 0, trigger, targetLevel);
	}
	level_loadedLevel->specialX = foundX;
	level_loadedLevel->specialY = foundY;
}

void preventObjectsAround(uint8_t x, uint8_t y, uint8_t d) {
	uint8_t lx, ly, ix, iy;

	lx = x+d;
	ly = y+d;
	if (lx >= 32) lx =31;
	if (ly >= 32) ly =31;

	if (x < d) {
		x = 0;
	} else {
		x -= d;
	}

	if (y < d) {
		y = 0;
	} else {
		y -= d;
	}

	for (ix = x; ix <= lx; ix++) {
		for (iy = y; iy <= ly; iy++) {
			if (mapOutline[ix][iy] == 1) mapOutline[ix][iy] = 2;
		}
	}
}

//probability of intersection state: 1/(2**roomFreq), invert=0 means no room chance, 1 means room chance
void generateLevel(uint8_t wall, uint8_t roomFreq, uint8_t invertRoomFreq) {
	uint8_t i=0,j=0, x=0,y=0, i2, j2, corrDir;
	uint8_t roomx1,roomy1, roomx2,roomy2;
	uint8_t wallType;

	level_initCurrentLevel();

	//zero out internal fields
	for (;i<4;i++) {
		for (j=0;j<4;j++) {
			corridors[i][j] = 0b11;
		}
	}

	for (i=0;i<32;i++) {
		for (j=0;j<32;j++) {
			mapOutline[i][j] = 0;
		}
	}

	//place corridors along lower diagonals
	for (i=3;i>1;i--) {
		x = 0;
		y = i;
		corrDir = 1 << (rand() & 1);
		while (y < 4) {
			if (rand() & 0b111) corridors[x][y] = corrDir;

			++x;
			++y;
		}
	}
	//and along upper diagonals
	for (i=0;i<4;i++) {
		x = i;
		y = 0;
		corrDir =1 << (rand() & 1);
		while (x < 4) {
			if (rand() & 0b111)corridors[x][y] = corrDir;

			++x;
			++y;
		}
	}

	//main room generation and population loop
	for (i=0;i<4; i++) {
		for (j=0;j<4;j++) {
			i2 = i*6;
			j2 = j*6;
			roomx1 = i2 + 2;
			roomy1 = j2 + 2;
			roomx2 = i2+ 3;
			roomy2 = j2 + 3;

			x = rand() & ((1<<roomFreq)-1);
			if (invertRoomFreq) x = !x;

			if (x) { // half the rooms are just corridor intersections
				roomx1 -= (rand()&1)+1;
				roomy1 -= (rand()&1)+1;

				roomx2 += rand3();
				roomy2 += rand3();
			}

			//draw room
			for(x=roomx1;x<roomx2;x++) {
				for (y=roomy1;y<roomy2;y++) {
					mapOutline[x][y] = 1;
				}
			}

			//draw corridors
			if ((corridors[i][j] & CORRIDOR_UP) && j>0) {
				x = i2 + 2;
				y = j2 + 2;
				while( y >= j2 - 4) {
					mapOutline[x][y] = 1;
					--y;
				}
			}

			if ((corridors[i][j] & CORRIDOR_RIGHT) && i<3) {
				x = i2 + 2;
				y = j2 + 2;
				while( x <= i2+ 8) {
					mapOutline[x][y] = 1;
					++x;
				}
			}
		}
	}

	//uncomment to draw map on screen
	/*VERA.address_hi = 0b00010000;
	VERA.address = 0;
	for (i=0;i<32;i++) {
		VERA.address = 320*i;
		for (j=0;j<32;j++) {
			VERA.data0 = mapOutline[j][i];
			//if (mapOutline[i][j]) inventory_addItem(1);
		}

	}
	while(1);*/


	//create level from outline
	for (i=0;i<32;i++) {
		for (j=0;j<32;j++) {
			level_setWall(i,j, WALL_RIGHT, 0);
			level_setWall(i,j, WALL_LEFT, 0);
			level_setWall(i,j, WALL_UP, 0);
			level_setWall(i,j, WALL_DOWN, 0);
			if (mapOutline[i][j]) {
				wallType = wall & 0b00011111;
				x = (i+(j<<1))&0b11;
				if ( x < 3) wallType += x;

				if (i==31 || !mapOutline[i+1][j]) level_setWall(i,j, WALL_RIGHT, wallType);
				if (i==0 || !mapOutline[i-1][j]) level_setWall(i,j, WALL_LEFT, wallType);
				if (j==31 || !mapOutline[i][j+1]) level_setWall(i,j, WALL_DOWN, wallType);
				if (j==0 || !mapOutline[i][j-1]) level_setWall(i,j, WALL_UP, wallType);
			}
		}
	}
}

const uint8_t area1ItemsPreset[9] = {
	ITEM_SWORD_2,
	ITEM_ARMOR_2,
	ITEM_CRAFT_BONE,
	ITEM_CRAFT_PLANT,
	ITEM_SCROLL_4,
	ITEM_MISC_POTION,
	ITEM_MISC_POTION,
	ITEM_MISC_POTION,
	ITEM_CRAFT_CRYSTAL
};

const uint8_t area2ItemsPreset[9] = {
	ITEM_SWORD_3,
	ITEM_ARMOR_3,
	ITEM_CRAFT_CRYSTAL,
	ITEM_CRAFT_CRYSTAL,
	ITEM_CRAFT_PLANT,
	ITEM_SCROLL_1,
	ITEM_MISC_POTION,
	ITEM_MISC_POTION,
	ITEM_CRAFT_BONE
};

const uint8_t area3ItemsPreset[9] = {
	ITEM_SWORD_4,
	ITEM_ARMOR_4,
	ITEM_CRAFT_CRYSTAL,
	ITEM_CRAFT_CRYSTAL,
	ITEM_CRAFT_PLANT,
	ITEM_CRAFT_PLANT,
	ITEM_CRAFT_BONE,
	ITEM_MISC_POTION,
	ITEM_MISC_POTION
};

uint8_t area1Items[9];
uint8_t area2Items[9];
uint8_t area3Items[9];

void mapgen_generateWorld() {
	uint8_t i;
	memcpy(area1Items, area1ItemsPreset, 9);
	memcpy(area2Items, area2ItemsPreset, 9);
	memcpy(area3Items, area3ItemsPreset, 9);
	utils_shuffleList(area1Items, 3);
	utils_shuffleList(area2Items, 3);
	utils_shuffleList(area3Items, 3);

	//first area
	//Level 1
	level_changeLevelFast(0);
	generateLevel(WALLSET_BASE_1, 1, 0);
	level_loadedLevel->ceilingColor = 0x45;
	level_loadedLevel->distanceColor = 0x66;
	level_loadedLevel->floorColor = 0x59;
	level_loadedLevel->music = SOUND_MUSIC_AREA_1;
	placeDoors(WALLSET_BASE_1, DOOR_FULL, DOOR_FULL);
	for (i=0; i<3; i++) placeItem(area1Items[i]);
	preventObjectsAround(level_loadedLevel->entranceX, level_loadedLevel->entranceY, 6);
	placeMonster(MONSTER_TYPE_ZOMBIE, 3);


	//Level 2
	level_changeLevelFast(1);
	generateLevel(WALLSET_BASE_1, 1, 0);
	level_loadedLevel->ceilingColor = 0x45;
	level_loadedLevel->distanceColor = 0x66;
	level_loadedLevel->floorColor = 0x59;
	level_loadedLevel->music = SOUND_MUSIC_AREA_1;
	placeDoors(WALLSET_BASE_1, DOOR_FULL, DOOR_FULL);
	placeMonster(MONSTER_TYPE_ZOMBIE, 5);
	for (i=3; i<6; i++) placeItem(area1Items[i]);

	//Level 3
	level_changeLevelFast(2);
	generateLevel(WALLSET_BASE_1, 2, 0);
	level_loadedLevel->ceilingColor = 0x45;
	level_loadedLevel->distanceColor = 0x66;
	level_loadedLevel->floorColor = 0x59;
	level_loadedLevel->music = SOUND_MUSIC_AREA_1;
	placeDoors(WALLSET_BASE_1, DOOR_FULL, DOOR_FULL);
	placePortal(28 | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT, ITEM_MISC_GHOST_KEY, 11, 1);
	placeMonster(MONSTER_TYPE_ZOMBIE, 4);
	placeMonster(MONSTER_TYPE_MAGE, 1);
	for (i=7; i<9; i++) placeItem(area1Items[i]);
	findObjectSpot();
	objects_spawnVendor(foundX, foundY, 30, area1Items[6]);

	//second area
	//Level 4
	level_changeLevelFast(3);
	generateLevel(WALLSET_BASE_2, 2, 0);
	level_loadedLevel->ceilingColor = 0xd8;
	level_loadedLevel->distanceColor = 0x11;
	level_loadedLevel->floorColor = 0x16;
	level_loadedLevel->music = SOUND_MUSIC_AREA_2;
	placeDoors(WALLSET_BASE_2, DOOR_FULL, DOOR_FULL);
	placeMonster(MONSTER_TYPE_MAGE, 5);
	for (i=0; i<3; i++) placeItem(area2Items[i]);
	findObjectSpot();
	objects_spawnCraftingTable(foundX, foundY);

	//Level 5
	level_changeLevelFast(4);
	generateLevel(WALLSET_BASE_2, 2, 1);
	level_loadedLevel->ceilingColor = 0xd8;
	level_loadedLevel->distanceColor = 0x11;
	level_loadedLevel->floorColor = 0x16;
	level_loadedLevel->music = SOUND_MUSIC_AREA_2;
	placeDoors(WALLSET_BASE_2, DOOR_FULL, DOOR_FULL);
	placeMonster(MONSTER_TYPE_MAGE, 5);
	for (i=3; i<6; i++) placeItem(area2Items[i]);
	placePortal(11, ITEM_MISC_PORTAL_KEY, 9, 0);
	findObjectSpot();
	objects_spawnCraftingTable(foundX, foundY);


	//Level 6
	level_changeLevelFast(5);
	generateLevel(WALLSET_BASE_2, 2, 0);
	level_loadedLevel->ceilingColor = 0xd8;
	level_loadedLevel->distanceColor = 0x11;
	level_loadedLevel->floorColor = 0x16;
	level_loadedLevel->music = SOUND_MUSIC_AREA_2;
	placeDoors(WALLSET_BASE_2, DOOR_FULL, DOOR_FULL);
	placeMonster(MONSTER_TYPE_MAGE, 3);
	placeMonster(MONSTER_TYPE_DEMON, 1);
	for (i=7; i<9; i++) placeItem(area2Items[i]);
	findObjectSpot();
	objects_spawnVendor(foundX, foundY, 40, area2Items[6]);

	//third area
	//Level 7
	level_changeLevelFast(6);
	generateLevel(WALLSET_BASE_3, 2, 1);
	level_loadedLevel->ceilingColor = 0x4b;
	level_loadedLevel->distanceColor = 0x11;
	level_loadedLevel->floorColor = 0xd2;
	level_loadedLevel->music = SOUND_MUSIC_AREA_3;
	placeDoors(WALLSET_BASE_3, DOOR_FULL, DOOR_FULL);
	placeMonster(MONSTER_TYPE_DEMON, 5);
	for (i=0; i<3; i++) placeItem(area3Items[i]);
	findObjectSpot();
	objects_spawnCraftingTable(foundX, foundY);

	//Level 8
	level_changeLevelFast(7);
	generateLevel(WALLSET_BASE_3, 1, 0);
	level_loadedLevel->ceilingColor = 0x4b;
	level_loadedLevel->distanceColor = 0x11;
	level_loadedLevel->floorColor = 0xd2;
	level_loadedLevel->music = SOUND_MUSIC_AREA_3;
	placeDoors(WALLSET_BASE_3, DOOR_FULL, DOOR_FULL);
	placeMonster(MONSTER_TYPE_DEMON, 5);
	for (i=3; i<5; i++) placeItem(area3Items[i]);
	findObjectSpot();
	objects_spawnVendor(foundX, foundY, 70, area3Items[5]);

	//Level 9
	level_changeLevelFast(8);
	generateLevel(WALLSET_BASE_3, 2, 0);
	level_loadedLevel->ceilingColor = 0x4b;
	level_loadedLevel->distanceColor = 0x11;
	level_loadedLevel->floorColor = 0xd2;
	level_loadedLevel->music = SOUND_MUSIC_AREA_3;
	placeDoors(WALLSET_BASE_3, DOOR_NONE, DOOR_FULL);
	if (!player_isEasyMode) placePortal(22, ITEM_MISC_GOLD_KEY, 10, 0); //true ending only available in normal mode
	placeMonster(MONSTER_TYPE_DEMON, 3);
	placeMonster(MONSTER_TYPE_DEMON_BOSS, 1);
	for (i=6; i<8; i++) placeItem(area3Items[i]);

	//area 10
	level_changeLevelFast(9);
	generateLevel(WALLSET_BASE_4, 2, 0);
	level_loadedLevel->ceilingColor = 0x10;
	level_loadedLevel->distanceColor = 0x10;
	level_loadedLevel->floorColor = 0x30;
	level_loadedLevel->music = SOUND_MUSIC_AREA_4;
	//placeDoors(WALLSET_BASE_4, DOOR_NONE, DOOR_DECORATIVE);
	placePortal(21, ITEM_MISC_PORTAL_KEY, 4, 0);
	preventObjectsAround(level_loadedLevel->specialX, level_loadedLevel->specialY, 4);
	placeMonster(MONSTER_TYPE_DEMON, 5);
	if (!player_isEasyMode) placeMonster(MONSTER_TYPE_ZOMBIE, 1); //true ending only available in normal mode
	placeMonster(MONSTER_TYPE_NECRO_BOSS, 1);
	for (i=0;i<3;i++) placeItem(ITEM_CRAFT_BONE);
	if (!player_isEasyMode) {
		placeItem(ITEM_MISC_WAND);
		placeItem(ITEM_SCROLL_3);
	}
	placeItem(ITEM_CRAFT_SPECIAL);

	//area 11
	level_changeLevelFast(10);
	generateLevel(WALLSET_BASE_3, 2, 0);
	level_loadedLevel->ceilingColor = 0x4b;
	level_loadedLevel->distanceColor = 0x11;
	level_loadedLevel->floorColor = 0x7c;
	level_loadedLevel->music = SOUND_MUSIC_AREA_5;
	placePortal(22, ITEM_MISC_GOLD_KEY, 8, 0);
	preventObjectsAround(level_loadedLevel->specialX, level_loadedLevel->specialY, 6);
	placeMonster(MONSTER_TYPE_SHADE, 3);
	placeItem(ITEM_SCROLL_2);
	placeItem(ITEM_MISC_GHOST_KEY);
	placeItem(ITEM_CRAFT_CRYSTAL);
	placeItem(ITEM_MISC_POTION);

	//area 12
	level_changeLevelFast(11);
	generateLevel(WALLSET_BASE_5, 2, 1);
	level_loadedLevel->ceilingColor = 0xb4;
	level_loadedLevel->distanceColor = 0x10;
	level_loadedLevel->floorColor = 0x12;
	placePortal(28 | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT, ITEM_MISC_GHOST_KEY, 2, 1);
	preventObjectsAround(level_loadedLevel->specialX, level_loadedLevel->specialY, 4);
	for (i=0;i<5;i++) {
		findObjectSpot();
		object_spawnGuard(foundX, foundY, i);
	}
}
