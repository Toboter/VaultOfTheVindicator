/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __GAME
#define __GAME

typedef enum {
	GAME_MODE_TITLE,
	GAME_MODE_NEW_GAME,
	GAME_MODE_PLAY,
	GAME_MODE_PAUSED,
	GAME_MODE_GAME_OVER,
	GAME_MODE_END1,
	GAME_MODE_END2,
	GAME_MODE_END3,
	GAME_MODE_END4
} GAME_MODE;

extern void game_changeMode(GAME_MODE mode);
extern void game_run();

#endif
