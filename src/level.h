/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __LEVEL
#define __LEVEL

#include <stdint.h>
#include "utils.h"

#define LEVEL_SIZE 32
#define OBJECT_COUNT 64

#define FIRST_LEVEL_BANK 5

typedef enum {
	WALL_UP = 0,
	WALL_RIGHT = 1,
	WALL_DOWN = 2,
	WALL_LEFT = 3
} WALL_ORIENTATION;


#define WALL_FLAG_TYPE 0b00011111
#define WALL_FLAG_TRANSPARENT 0b00100000
#define WALL_FLAG_OBJECT 0b01000000

#define COLLISION_NONE 0
#define COLLISION_OBJECT 1

#define OBJECT_TYPE_ID 0b00001111
#define OBJECT_TYPE_HAS_COLLISION 0b00010000
#define OBJECT_TYPE_VISUAL_PRIORITY 0b00100000

#define OBJECT_ID_DECORATION 0
#define OBJECT_ID_ITEM 1
#define OBJECT_ID_MONSTER 2
#define OBJECT_ID_MONEY_BAG 3
#define OBJECT_ID_CRAFTING_TABLE 4
#define OBJECT_ID_VENDOR 5
#define OBJECT_ID_UP_DOOR 6
#define OBJECT_ID_DOWN_DOOR 7
#define OBJECT_ID_PORTAL_DOOR 8
#define OBJECT_ID_GUARD 9


//fields with an _ should not be modified directly
typedef struct {
	uint8_t _exists;
	uint8_t _objectType;
	uint8_t _x;
	uint8_t _y;
	uint8_t wallType; //which wall the object should be represented by

	uint8_t data[11];
} Object;

typedef struct {
	uint8_t wallData[LEVEL_SIZE][LEVEL_SIZE][4]; //walls are included in order up,right,down,left

	uint8_t collisionMap[LEVEL_SIZE][LEVEL_SIZE]; //bool whether there's an object in a certain position

	Object objects[OBJECT_COUNT];

	uint8_t minimap[16][16];

	uint8_t entranceX;
	uint8_t entranceY;
	uint8_t exitX;
	uint8_t exitY;
	uint8_t specialX;
	uint8_t specialY;

	uint8_t ceilingColor;
	uint8_t distanceColor;
	uint8_t floorColor;

	uint8_t music;
} Level;

extern Level * const level_loadedLevel; //pointer to hiram
extern uint8_t level_currentLevel;
extern uint8_t level_currentLevelBank; //contains the ram bank of the currently loaded level

extern uint8_t level_currentObjectInteracted; // 1 if the currently update object has been interacted with by the player

extern void level_initHiram();

extern void level_initCurrentLevel();
extern void level_changeLevel(uint8_t nextLevel, uint8_t isEntrance);
extern void level_changeLevelFast(uint8_t nextLevel); //only to be called before the first frame has been rendered

extern Object *  level_allocNewObject(uint8_t x, uint8_t y, uint8_t objectType);
extern void  level_deleteObject(Object * object);
extern uint8_t  level_moveObject(Object * object, uint8_t x, uint8_t y); //performs an object collision check, but not a wall collision check

extern void level_doObjectWalls(uint8_t remove); //remove = 0: place, remove = 1: remove
extern void level_updateObjects();

extern uint8_t  level_getWall(uint8_t x, uint8_t y, WALL_ORIENTATION orientation);
extern void  level_setWall(uint8_t x, uint8_t y, WALL_ORIENTATION orientation, uint8_t wall); //assumes ram bank is correctly set

extern void level_updateMinimap(uint8_t x, uint8_t y);

#endif
