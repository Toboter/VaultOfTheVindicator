/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "objects.h"

#include <stdlib.h>

#include "level.h"
#include "inventory.h"
#include "items.h"
#include "player.h"
#include "hud.h"
#include "rendering.h"
#include "game.h"
#include "sound.h"


void objects_spawnDecoration(uint8_t x, uint8_t y, uint8_t wallType) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_DECORATION);
	if (obj) {
		obj->wallType = wallType | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
	}
}


void objects_spawnMoneyBag(uint8_t x, uint8_t y) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_MONEY_BAG);
	if (obj) {
		obj->wallType = 0x13 | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
	}
}

void objects_updateMoneyBag(Object * obj) {
	if (level_currentObjectInteracted) {
		player_onInteract();
		if (player_gold < 99) {
			sound_playSFX(SOUND_EFFECT_BOOP, SOUND_PRIORITY_ACTION);
			player_gold += (rand()&7) + 3;
			if (player_gold > 99) player_gold = 99;
			level_deleteObject(obj);
		}
	}
}



#define CRAFTING_STATE 0
#define CRAFTING_ITEM 1

#define CRAFTING_STATE_NO_ITEM 0
#define CRAFTING_STATE_ONE_ITEM 1


uint8_t craftingRecipes[4][4] = {
		{ITEM_SCROLL_2, ITEM_MISC_POTION,	ITEM_MISC_WAND,		ITEM_CRAFT_SPECIAL},
		{ITEM_MISC_POTION, ITEM_SCROLL_1, ITEM_SCROLL_3, ITEM_CRAFT_SPECIAL},
		{ITEM_MISC_WAND, ITEM_SCROLL_3, 	ITEM_SCROLL_4, 	ITEM_MISC_PORTAL_KEY},
		{ITEM_CRAFT_SPECIAL, ITEM_CRAFT_SPECIAL, ITEM_MISC_PORTAL_KEY, ITEM_CRAFT_SPECIAL}
};

void objects_spawnCraftingTable(uint8_t x, uint8_t y) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_CRAFTING_TABLE);
	if (obj) {
		obj->wallType = 0x14 | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
		obj->data[CRAFTING_STATE] = CRAFTING_STATE_NO_ITEM;
		obj->data[CRAFTING_ITEM] = 0;
	}
}

void object_updateCraftingTable(Object * obj) {
	uint8_t item;
	if (level_currentObjectInteracted) {
		player_onInteract();
		switch (obj->data[CRAFTING_STATE]) {
			case CRAFTING_STATE_NO_ITEM:
				if (ITEM_GET_TYPE(inventory_getSelectedItem()) == ITEM_TYPE_CRAFTABLE) {
					obj->data[CRAFTING_ITEM] = inventory_getSelectedItem();
					inventory_popCurrentItem();
					obj->data[CRAFTING_STATE] = CRAFTING_STATE_ONE_ITEM;
					obj->wallType = 0x16 | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
					sound_playSFX(SOUND_EFFECT_MAGIC_ATTACK, SOUND_PRIORITY_ACTION);
				} else {
					hud_print("Some arcane workstation maybe?");
					utils_sleep(60);
				}
				break;

			case CRAFTING_STATE_ONE_ITEM:
				if (ITEM_GET_TYPE(inventory_getSelectedItem()) == ITEM_TYPE_CRAFTABLE) {
					hud_displayEffect(HUD_EFFECT_MAGIC);
					item = craftingRecipes[obj->data[CRAFTING_ITEM] - ITEM_CRAFT_CRYSTAL][inventory_popCurrentItem() - ITEM_CRAFT_CRYSTAL];
					inventory_addItem(item);
					hud_print(items_itemNames[item]);
					sound_playSFX(SOUND_EFFECT_MAGIC_CAST, SOUND_PRIORITY_ACTION);
					utils_sleep(30);
					obj->data[CRAFTING_STATE] = CRAFTING_STATE_NO_ITEM;
					obj->wallType = 0x14 | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
				} else {
					if (inventory_addItem(obj->data[CRAFTING_ITEM])) {
						sound_playSFX(SOUND_EFFECT_BOOP, SOUND_PRIORITY_ACTION);
						hud_print(items_itemNames[obj->data[CRAFTING_ITEM]]);
						utils_sleep(30);
						obj->data[CRAFTING_STATE] = CRAFTING_STATE_NO_ITEM;
						obj->wallType = 0x14 | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
					}
				}
		}
	}
}

#define VENDOR_PRICE 0
#define VENDOR_ITEM 1
#define VENDOR_STATE 2
void objects_spawnVendor(uint8_t x, uint8_t y, uint8_t price, uint8_t item) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_VENDOR);
	if (obj) {
		obj->wallType = 0x15 | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
		obj->data[VENDOR_PRICE] = price;
		obj->data[VENDOR_ITEM] = item;
		obj->data[VENDOR_STATE] = 0;
	}
}

void objects_updateVendor(Object * obj) {
	uint8_t price = obj->data[VENDOR_PRICE];
	if (level_currentObjectInteracted) {
		player_onInteract();
		if (obj->data[VENDOR_STATE] == 0 || player_gold < price) {
			hud_print("Buy ");
			hud_print(items_itemNames[obj->data[VENDOR_ITEM]]);
			hud_print(" for ");
			hud_printNumber(price);
			hud_print(" gold?");
			sound_playSFX(SOUND_EFFECT_BOOP, SOUND_PRIORITY_ACTION);
			utils_sleep(60);
			obj->data[VENDOR_STATE] = 1;

		} else if (player_gold >= price) {
			player_gold -= price;
			sound_playSFX(SOUND_EFFECT_MAGIC_CAST, SOUND_PRIORITY_ACTION);
			hud_displayEffect(HUD_EFFECT_MAGIC);
			hud_print("Thank you, devout");
			utils_sleep(30);
			level_deleteObject(obj);
			items_spawnItemObject(obj->_x, obj->_y, obj->data[VENDOR_ITEM]);
		}
	}
}


void objects_spawnUpDoorTrigger(uint8_t x, uint8_t y) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_UP_DOOR);
	if (obj) {
		obj->wallType = 0;
	}
}

void objects_spawnDownDoorTrigger(uint8_t x, uint8_t y) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_DOWN_DOOR);
	if (obj) {
		obj->wallType = 0;
	}
}

#define PORTAL_WALL 0
#define PORTAL_TRIGGER 1
#define PORTAL_TARGETLEVEL 2
void objects_spawnPortalDoorTrigger(uint8_t x, uint8_t y, uint8_t wallType, uint8_t triggerItem, uint8_t targetLevel) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_PORTAL_DOOR);
	if (obj) {
		obj->wallType = 0;
		obj->data[PORTAL_WALL] = wallType;
		obj->data[PORTAL_TRIGGER] = triggerItem;
		obj->data[PORTAL_TARGETLEVEL] = targetLevel;
	}
}

char * levelNames[] = {
	"Level 1\nRuins",
	"Level 2\nRuins",
	"Level 3\nRuins",
	"Level 4\nTemple",
	"Level 5\nTemple",
	"Level 6\nTemple",
	"Level 7\nCloister",
	"Level 8\nCloister",
	"Level 9\nCloister's Vestibule",
	"Level ?\nThe Fissure",
	"Level 10\nThe Chambers",
	"Level ?\n???"
};

void objects_printLevelHeader() {
	hud_print(levelNames[level_currentLevel]);
}

void object_updateUpDoor() {
	if (level_currentObjectInteracted) {
		player_onInteract();
		if (level_currentLevel == 0) {
			switch (player_progress) {
				case 0:
					hud_print("I'm not leaving 'til we're safe");
					utils_sleep(60);
					break;
				case 1:
				case 2:
					hud_print("I have to find him");
					utils_sleep(60);
					break;
				case 3:
					game_changeMode(GAME_MODE_END4);
					break;
			}
			return;
		}

		sound_playSFX(SOUND_EFFECT_STAIRS, SOUND_PRIORITY_ACTION);
		level_changeLevel(level_currentLevel - 1, 0);
		SET_RAM_BANK(level_currentLevelBank);
		objects_printLevelHeader();
		utils_sleep(60);
		sound_playMusic(level_loadedLevel->music);
	}
}

void object_updateDownDoor() {
	if (level_currentObjectInteracted) {
		player_onInteract();
		sound_playSFX(SOUND_EFFECT_STAIRS, SOUND_PRIORITY_ACTION);
		level_changeLevel(level_currentLevel + 1, 1);
		SET_RAM_BANK(level_currentLevelBank);
		objects_printLevelHeader();
		utils_sleep(60);
		sound_playMusic(level_loadedLevel->music);
	}
}

void object_updatePortalDoor(Object * obj) {
	if (player_progress >= 2) {
		obj->wallType = obj->data[PORTAL_WALL];
	} else {
		obj->wallType = 0;
	}

	if (level_currentObjectInteracted && inventory_getSelectedItem() == obj->data[PORTAL_TRIGGER]) {
		player_onInteract();
		sound_playSFX(SOUND_EFFECT_HIT, SOUND_PRIORITY_ACTION);
		level_changeLevel(obj->data[PORTAL_TARGETLEVEL], 2);
		SET_RAM_BANK(level_currentLevelBank);

		if (player_progress < 1) player_progress = 1;

		objects_printLevelHeader();
		utils_sleep(60);
		sound_playMusic(level_loadedLevel->music);
	}

}


char * guardTexts[] = {
	"Please save me, mom. Let's leave.",
	"Thank you for saving us",
	"Bless the Arbiter, we're safe!",
	"Please, help us get out of here!",
	"You're here to save us, aren't you?"
};

void object_spawnGuard(uint8_t x, uint8_t y, uint8_t type) {
	Object * obj = level_allocNewObject(x, y, OBJECT_ID_GUARD);
	if (obj) {
		obj->wallType = (type ? 31 : 30) | WALL_FLAG_OBJECT | WALL_FLAG_TRANSPARENT;
		obj->data[0] = type;
	}
}

void object_updateGuard(Object * obj) {
	if (level_currentObjectInteracted) {
		player_onInteract();
		if (obj->data[0] == 0 && player_progress < 3) player_progress = 3;
		hud_print(guardTexts[obj->data[0]]);
		sound_playSFX(SOUND_EFFECT_HIT, SOUND_PRIORITY_ACTION);
		utils_sleep(20);
		sound_playSFX(SOUND_EFFECT_HIT, SOUND_PRIORITY_ACTION);
		utils_sleep(100);
	}
}
