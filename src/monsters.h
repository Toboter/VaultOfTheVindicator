/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __MONSTERS
#define __MONSTERS

#include <stdint.h>
#include "level.h"

#define MONSTER_EFFECT_CHARM 0b1
#define MONSTER_EFFECT_POISON 0b10

#define MONSTER_DATA_TYPE 0
#define MONSTER_DATA_HP 1
#define MONSTER_DATA_ATTACK 2
#define MONSTER_DATA_DEFENSE 3
#define MONSTER_DATA_STATE 4
#define MONSTER_DATA_EFFECTS 5
#define MONSTER_DATA_EFFECT_TIMER 6

#define MONSTER_TYPE_ZOMBIE 0
#define MONSTER_TYPE_MAGE 1
#define MONSTER_TYPE_DEMON 2
#define MONSTER_TYPE_DEMON_BOSS 3
#define MONSTER_TYPE_NECRO_BOSS 4
#define MONSTER_TYPE_SHADE 5

extern void monsters_spawnMonster(uint8_t x, uint8_t y, uint8_t monsterType);
extern void monsters_updateMonster(Object * mon);
extern void monsters_applyEffect(Object * mon, uint8_t effect, uint8_t duration);

#endif
