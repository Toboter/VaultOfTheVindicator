/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "hud.h"


#include <stdint.h>
#include <cx16.h>
#include <cbm.h>

#include "utils.h"


#define TILESET_BASE 0x3000
#define TILESET_8x8 0x13000
#define TILESET_8x8_SPRITE_BASE 0x980u//lowest 5 bits missing
#define TILESET_16x16_SPRITE_BASE 0xa80u//lowest 5 bits missing
#define TILESET_16x16 0x15000

#define SPRITE_TEXT_R1 0 //20
#define SPRITE_TEXT_R2 20 //20

#define SPRITE_ICON_GOLD 40
#define SPRITE_ICON_HP 41
#define SPRITE_ICON_STR 42
#define SPRITE_ICON_MAG 43
#define SPRITE_ICON_DEF 44

#define SPRITE_INVENTORY 57 //6

#define SPRITE_MAP 65
#define SPRITE_MAP_BG 66
#define SPRITE_MAP_CURSOR 64
#define SPRITE_COMPASS_DIR_LEFT 70
#define SPRITE_COMPASS_DIR_FORW 71
#define SPRITE_COMPASS_DIR_RIGHT 72
#define SPRITE_COMPASS_ARM_1 73
#define SPRITE_COMPASS_ARM_2 74


#define INVENTORY_X 131
#define INVENTORY_Y 11
#define INVENTORY_STEP 17

#define MAP_X 15
#define MAP_Y 78

#define COMPASS_X 80
#define COMPASS_Y 13


#define HUD_COLOR_OFFSET 8


#define VERA_SPRITE_ADDR 0xfc00



void setup8x8Sprites(uint8_t startSprite, uint8_t x, uint8_t y, uint16_t amount, uint8_t xincr, uint8_t paletteOff) {
	uint8_t i=0;
	VERA.address_hi = 0b00010001; //increment 1, hi addr 1
	VERA.address = VERA_SPRITE_ADDR + (startSprite<<3);
	for (;i<amount;i++, x+=xincr) {
		VERA.data0 = ((TILESET_8x8_SPRITE_BASE) & 0xff); //addr 12:5
		VERA.data0 = ((TILESET_8x8_SPRITE_BASE)>>8) | 0b10000000; //8bpp, addr 16:13
		VERA.data0 = x; //x 7:0
		VERA.data0 = 0;//x 9:8
		VERA.data0 = y;//y 7:0
		VERA.data0 = 0;// y 9:8
		VERA.data0 = 0b00001100; //collision mask 0, zdepth before layer 1, no flip
		VERA.data0 = paletteOff; //width/height 8px, palette offset 0
	}
}

void setup16x16Sprites(uint8_t startSprite, uint8_t x, uint8_t y, uint16_t amount, uint8_t yincr, uint8_t paletteOff) {
	uint8_t i=0;
	VERA.address_hi = 0b00010001; //increment 1, hi addr 1
	VERA.address = VERA_SPRITE_ADDR + (startSprite<<3);
	for (;i<amount;i++, y+=yincr) {
		VERA.data0 = ((TILESET_16x16_SPRITE_BASE) & 0xff); //addr 12:5
		VERA.data0 = ((TILESET_16x16_SPRITE_BASE)>>8) | 0b10000000; //8bpp, addr 16:13
		VERA.data0 = x; //x 7:0
		VERA.data0 = 0;//x 9:8
		VERA.data0 = y;//y 7:0
		VERA.data0 = 0;// y 9:8
		VERA.data0 = 0b00001100; //collision mask 0, zdepth before layer 1, no flip
		VERA.data0 = paletteOff | 0b01010000; //width/height 16px, palette offset 0
	}
}

void setSpriteTile(uint8_t spriteIndex, uint8_t tile, uint8_t is16) {
	uint16_t tileAddr;
	VERA.address_hi = 0b00010001; //increment 1, hi addr 1
	VERA.address = VERA_SPRITE_ADDR + (spriteIndex<<3);
	tileAddr = is16 ? TILESET_16x16_SPRITE_BASE + (tile<<3) : TILESET_8x8_SPRITE_BASE + (tile<<1);
	VERA.data0 = (tileAddr) & 0xff; //addr 12:5
	VERA.data0 = (tileAddr>>8) | 0b10000000; //8bpp, addr 16:13
}

//public functions
uint8_t textPointer = SPRITE_TEXT_R1; // where in the print are we rn?

void   hud_print(char * str) {
	char c = 0;
	while (*str != '\0' && textPointer < SPRITE_TEXT_R2+20) {
		c = *str;
		if (c >= 0x80) c-=0x80;
		if (c == '\n') {
			if (textPointer < SPRITE_TEXT_R2) textPointer = SPRITE_TEXT_R2;
			str++;
			continue;
		}
		setSpriteTile(textPointer, c-0x20, 0);

		str++;
		textPointer++;
	}
}

char buff[2] = "0\0";

void   hud_printNumber(uint8_t num) {
	if (num >= 100) {
		buff[0] = 0x30 + (num/100);
		hud_print(buff);
	}
	if (num >= 10) {
		buff[0] = 0x30 + ((num%100)/10);
		hud_print(buff);
	}
	buff[0] = 0x30 + (num%10);
	hud_print(buff);
}

void hud_clearText() {
	uint8_t i=SPRITE_TEXT_R1;
	textPointer = SPRITE_TEXT_R1;
	for (;i<SPRITE_TEXT_R1+40;i++) setSpriteTile(i, 0, 0);
}

void hud_setDisplayValue(uint8_t valueId, uint8_t value) {
	if (value >= 10) {
		setSpriteTile(valueId, 0x10 + (value/10), 0);
	} else {
		setSpriteTile(valueId, 0, 0);
	}
	setSpriteTile(valueId+1, 0x10 + (value%10), 0);
}

void hud_setInventorySlot(uint8_t slot, uint8_t value) {
	setSpriteTile(SPRITE_INVENTORY + slot, value, 1);
}

void hud_setInventoryCursor(uint8_t cursor, uint8_t slot) {
	VERA.address_hi = 0b00010001; //increment 1, hi addr 1
	VERA.address = VERA_SPRITE_ADDR + (cursor<<3) + 4;

	VERA.data0 = INVENTORY_Y + (INVENTORY_STEP * slot);//y 7:0
}

void hud_setMapCursor(uint8_t x, uint8_t y) {
	VERA.address_hi = 0b00010001; //increment 1, hi addr 1
	VERA.address = VERA_SPRITE_ADDR + (SPRITE_MAP_CURSOR<<3) + 2;

	VERA.data0 = MAP_X + x+1;
	VERA.data0 = 0;
	VERA.data0 = MAP_Y + y+1;
	VERA.data0 = 0;
}

void hud_setCompassCursor(uint8_t rotation) {
	setSpriteTile(SPRITE_COMPASS_DIR_FORW, 0x66 + rotation, 0);
	setSpriteTile(SPRITE_COMPASS_DIR_RIGHT, 0x66 + ( (rotation+1) & 0b11), 0);
	setSpriteTile(SPRITE_COMPASS_DIR_LEFT, 0x66 + ( (rotation-1) & 0b11), 0);
	//setSpriteTile(SPRITE_COMPASS_CURSOR, 0x60 + rotation, 0);
}

void hud_displayEffect(uint8_t effect) {
	VERA.address_hi = 0b00010001; //increment 1, hi addr 1
	VERA.address = VERA_SPRITE_ADDR + (effect<<3) + 2;

	VERA.data0 = 73;
}

void  hud_hideEffect(uint8_t effect) {
	VERA.address_hi = 0b00010001; //increment 1, hi addr 1
	VERA.address = VERA_SPRITE_ADDR + (effect<<3) + 2;
	VERA.data0 = 200;
}

void hud_hideAllEffects() {
	hud_hideEffect(HUD_EFFECT_BLOOD);
	hud_hideEffect(HUD_EFFECT_SWORD);
	hud_hideEffect(HUD_EFFECT_MAGIC);
	hud_hideEffect(HUD_EFFECT_BLOODY_SWORD);
	hud_hideEffect(HUD_EFFECT_BLOODY_MAGIC);
}

void hud_setEnabled(uint8_t enabled) {
	SET_CTRL(0,0);
	VERA.display.video = (VERA.display.video & 0b10111111) | (enabled<<6);
}

void hud_init() {
	hud_setEnabled(0);
	cbm_k_setnam("graphics.bin");
	cbm_k_setlfs(0, 8, 0);
	cbm_k_load(3, TILESET_BASE);

	//textbox
	setup8x8Sprites(SPRITE_TEXT_R1, 31, 94, 20, 5, HUD_COLOR_OFFSET);
	setup8x8Sprites(SPRITE_TEXT_R2, 31, 101, 20, 5, HUD_COLOR_OFFSET);
	hud_clearText();

	//stats
	setup8x8Sprites(SPRITE_ICON_GOLD, 14, 30, 1, 0, 0);
	setup8x8Sprites(HUD_VALUE_GOLD, 19, 29, 2, 5, HUD_COLOR_OFFSET);
	setSpriteTile(SPRITE_ICON_GOLD, 0x5a, 0);
	hud_setDisplayValue(HUD_VALUE_GOLD, 0);

	setup8x8Sprites(SPRITE_ICON_HP, 14, 23, 1, 0, 0);
	setup8x8Sprites(HUD_VALUE_HP, 19, 22, 2, 5, HUD_COLOR_OFFSET);
	setSpriteTile(SPRITE_ICON_HP, 0x5b, 0);
	hud_setDisplayValue(HUD_VALUE_HP, 0);

	setup8x8Sprites(SPRITE_ICON_STR, 13, 37, 1, 0, 0);
	setup8x8Sprites(HUD_VALUE_STR, 19, 36, 2, 5, HUD_COLOR_OFFSET);
	setSpriteTile(SPRITE_ICON_STR, 0x5c, 0);
	hud_setDisplayValue(HUD_VALUE_STR, 0);

	setup8x8Sprites(SPRITE_ICON_MAG, 13, 44, 1, 0, 0);
	setup8x8Sprites(HUD_VALUE_MAG, 19, 43, 2, 5, HUD_COLOR_OFFSET);
	setSpriteTile(SPRITE_ICON_MAG, 0x5d, 0);
	hud_setDisplayValue(HUD_VALUE_MAG, 0);

	setup8x8Sprites(SPRITE_ICON_DEF, 13, 51, 1, 0, 0);
	setup8x8Sprites(HUD_VALUE_DEF, 19, 50, 2, 5, HUD_COLOR_OFFSET);
	setSpriteTile(SPRITE_ICON_DEF, 0x5e, 0);
	hud_setDisplayValue(HUD_VALUE_DEF, 0);

	//set up inventory sprites
	setup16x16Sprites(HUD_INV_CURSOR1, INVENTORY_X, INVENTORY_Y, 1, 0, 0);
	setup16x16Sprites(HUD_INV_CURSOR2, INVENTORY_X, 200, 1, 0, 0);
	setup16x16Sprites(SPRITE_INVENTORY, INVENTORY_X, INVENTORY_Y, 6, INVENTORY_STEP, 0);
	setSpriteTile(HUD_INV_CURSOR1, 0x18, 1);
	setSpriteTile(HUD_INV_CURSOR2, 0x19, 1);

	//map
	setup16x16Sprites(SPRITE_MAP_BG, MAP_X, MAP_Y, 1, 0, 0);
	setSpriteTile(SPRITE_MAP_BG, 0x16, 1);
	setup16x16Sprites(SPRITE_MAP, MAP_X+1, MAP_Y+1, 1, 0, 0);
	setSpriteTile(SPRITE_MAP, 0x1a, 1);
	setup8x8Sprites(SPRITE_MAP_CURSOR, MAP_X, MAP_Y, 1, 0, 0);
	setSpriteTile(SPRITE_MAP_CURSOR, 0x5f, 0);

	//setup hud cursor
	setup8x8Sprites(SPRITE_COMPASS_DIR_FORW, COMPASS_X-4, COMPASS_Y, 1, 0, HUD_COLOR_OFFSET);
	setup8x8Sprites(SPRITE_COMPASS_ARM_1, COMPASS_X-12, COMPASS_Y, 1, 0, HUD_COLOR_OFFSET);
	setup8x8Sprites(SPRITE_COMPASS_ARM_2, COMPASS_X, COMPASS_Y, 1, 0, HUD_COLOR_OFFSET);
	setup8x8Sprites(SPRITE_COMPASS_DIR_LEFT, COMPASS_X-17, COMPASS_Y+4, 1, 0, HUD_COLOR_OFFSET);
	setup8x8Sprites(SPRITE_COMPASS_DIR_RIGHT, COMPASS_X+9, COMPASS_Y+4, 1, 0, HUD_COLOR_OFFSET);
	setSpriteTile(SPRITE_COMPASS_DIR_FORW, 0x66, 0);
	setSpriteTile(SPRITE_COMPASS_DIR_RIGHT, 0x67, 0);
	setSpriteTile(SPRITE_COMPASS_DIR_LEFT, 0x69, 0);
	setSpriteTile(SPRITE_COMPASS_ARM_1, 0x64, 0);
	setSpriteTile(SPRITE_COMPASS_ARM_2, 0x65, 0);

	//effects
	setup16x16Sprites(HUD_EFFECT_BLOOD, 200, 53, 1, 0, 0);
	setup16x16Sprites(HUD_EFFECT_SWORD, 200, 53, 1, 0, 0);
	setup16x16Sprites(HUD_EFFECT_MAGIC, 200, 53, 1, 0, 0);
	setup16x16Sprites(HUD_EFFECT_BLOODY_SWORD, 200, 53, 1, 0, 0);
	setup16x16Sprites(HUD_EFFECT_BLOODY_MAGIC, 200, 53, 1, 0, 0);
	setSpriteTile(HUD_EFFECT_BLOOD, 0x1b, 1);
	setSpriteTile(HUD_EFFECT_SWORD, 0x1c, 1);
	setSpriteTile(HUD_EFFECT_MAGIC, 0x1d, 1);
	setSpriteTile(HUD_EFFECT_BLOODY_SWORD, 0x1e, 1);
	setSpriteTile(HUD_EFFECT_BLOODY_MAGIC, 0x1f, 1);
}
