/*
 * SPDX-FileCopyrightText: 2023 Toboter
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __OBJECTS
#define __OBJECTS

#include <stdint.h>

#include "level.h"

extern void objects_spawnDecoration(uint8_t x, uint8_t y, uint8_t wallType);

extern void objects_spawnMoneyBag(uint8_t x, uint8_t y);
extern void objects_updateMoneyBag(Object * obj);

extern void objects_spawnCraftingTable(uint8_t x, uint8_t y);
extern void object_updateCraftingTable(Object * obj);

extern void objects_spawnVendor(uint8_t x, uint8_t y, uint8_t price, uint8_t item);
extern void objects_updateVendor(Object * obj);


extern void objects_printLevelHeader();


extern void objects_spawnUpDoorTrigger(uint8_t x, uint8_t y);
extern void objects_spawnDownDoorTrigger(uint8_t x, uint8_t y);
extern void objects_spawnPortalDoorTrigger(uint8_t x, uint8_t y, uint8_t wallType, uint8_t triggerItem, uint8_t targetLevel);
extern void object_updateUpDoor();
extern void object_updateDownDoor();
extern void object_updatePortalDoor(Object * obj);

extern void object_spawnGuard(uint8_t x, uint8_t y, uint8_t type);
extern void object_updateGuard(Object * obj);
#endif
