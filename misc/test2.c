

#include <cx16.h>
#include <cbm.h>
#include <stdint.h>

uint8_t timeLow, timeHigh;
uint16_t rdtim() {
	asm volatile("jsr $ffde"); //rdtim
	asm volatile("sta %v", timeLow);
	asm volatile("stx %v", timeHigh);
	return (timeHigh<<8) | timeLow;
}

void utils_sleep(uint16_t time) {
	//return;
	cbm_k_settim(0);
	while(rdtim() < time) {}
}

int main() {
	uint16_t i;
	VERA.layer0.config = 0b00000111;//bitmap mode, color 8bpp#
	VERA.layer0.tilebase = 0b00;//bitmap addr 0, width 320x200

	VERA.control = 0;
	VERA.display.video |= 0b00010000;//enable  layer0
	VERA.display.video &= ~0b00100000;//disable layer1
	VERA.display.hscale = 32; //set divider to 160x120
	VERA.display.vscale = 32;

	VERA.address = 0;
	VERA.address_hi = 0b00010000;
	for (i=0;i<0xffffu;i++) {
		VERA.data0 = i >> 8;
	}
	for (i=0;i<0x3001;i++) {
		VERA.data0 = i >> 8;
	}

	//

	cbm_k_setnam("test2");
	cbm_k_setlfs(0, 8, 0);
	cbm_k_load(2, 0);
	cbm_k_load(2, 0xc000);
	cbm_k_load(3, 0);

	while (1) {
		VERA.layer0.tilebase = 0x13<<2;
		utils_sleep(60);
		VERA.layer0.tilebase = 0;
		utils_sleep(60);
	}
	return 0;
}
