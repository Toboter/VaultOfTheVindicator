import tkinter as tk
from tkinter import filedialog
import pygame as pg
import os
import random
import time




wallColors = ((255,255,255), (20,20,20), (255,20,20), (20,255,20), (20,20,255), (255,255,20), (20,255,255), (255,20,255), (128,20,20), (20,128,20), (20,20,128), (128,128,20), (20,128,128), (128,20,128), (128,128,128))


pgd = pg.draw

levelSize = 32
pixelPerTile = 20

pg.init()

window = pg.display.set_mode((levelSize*pixelPerTile,levelSize*pixelPerTile), pg.RESIZABLE)

pg.display.set_caption("Level Edit")

level = []


for i in range(levelSize):
    level.append([])
    for j in range(levelSize):
        level[-1].append([0,0,0,0])


root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()
if file_path:
    with open(file_path,"rb") as f:
        b = f.read()
        j = 0
        for x in range(levelSize):
            for y in range(levelSize):
                for i in range(4):
                    level[x][y][i] = b[j]
                    j+=1


font = pg.font.SysFont("Arial", 14)

cursorX = levelSize//2
cursorY = levelSize//2


selectedWall = 1
transparent = 0

while True:
    pressed = False
    for event in pg.event.get():
        if pg.event.event_name(event.type) in ("WindowClose","Quit"):
            exit(0)

    if pg.key.get_pressed()[pg.K_ESCAPE]:
        exit(0)

    if pg.key.get_pressed()[pg.K_w] and cursorY>0:
        cursorY -= 1
        pressed=True
    if pg.key.get_pressed()[pg.K_a] and cursorX>0:
        cursorX -= 1
        pressed=True
    if pg.key.get_pressed()[pg.K_s] and cursorY<levelSize-1:
        cursorY += 1
        pressed=True
    if pg.key.get_pressed()[pg.K_d] and cursorX<levelSize-1:
        cursorX += 1
        pressed=True

    if pg.key.get_pressed()[pg.K_KP_PLUS] and selectedWall < len(wallColors)-1:
        selectedWall+=1
        pressed=True
    if pg.key.get_pressed()[pg.K_KP_MINUS] and selectedWall >0:
        selectedWall-=1
        pressed=True

    if pg.key.get_pressed()[pg.K_UP]:
        level[cursorX][cursorY][0] = selectedWall | (transparent<<5)
        pressed=True
    if pg.key.get_pressed()[pg.K_RIGHT]:
        level[cursorX][cursorY][1] = selectedWall | (transparent<<5)
        pressed=True
    if pg.key.get_pressed()[pg.K_DOWN]:
        level[cursorX][cursorY][2] = selectedWall | (transparent<<5)
        pressed=True
    if pg.key.get_pressed()[pg.K_LEFT]:
        level[cursorX][cursorY][3] = selectedWall | (transparent<<5)
        pressed=True

    if pg.key.get_pressed()[pg.K_t]:
        transparent = (transparent+1)%2
        pressed=True

    if pg.key.get_pressed()[pg.K_p]:
        root = tk.Tk()
        root.withdraw()
        file_path = filedialog.asksaveasfilename(filetypes=(("Bank file", "*.bank"),))
        if not ".bank" in file_path:
            file_path+=".bank"
        with open(file_path, "wb") as f:
            for x in range(levelSize):
                for y in range(levelSize):
                    for i in level[x][y]:
                        f.write(i.to_bytes(1,"little"))
        print("Saved at "+file_path)


    window.fill((220,220,220))

    for x in range(levelSize):
        for y in range(levelSize):
            x1 = x*pixelPerTile
            y1 = y*pixelPerTile
            x2 = (x+1)*pixelPerTile-1
            y2 = (y+1)*pixelPerTile-1
            if level[x][y][0]:
                pgd.line(window, wallColors[level[x][y][0] & 0b11111], (x1, y1), (x2, y1))
            if level[x][y][1]:
                pgd.line(window, wallColors[level[x][y][1] & 0b11111] , (x2, y1), (x2, y2))
            if level[x][y][2]:
                pgd.line(window, wallColors[level[x][y][2] & 0b11111], (x1, y2), (x2, y2))
            if level[x][y][3]:
                pgd.line(window, wallColors[level[x][y][3] & 0b11111], (x1, y1), (x1, y2))

    pgd.circle(window, wallColors[selectedWall], ((cursorX+0.5)*pixelPerTile, (cursorY+0.5)*pixelPerTile), pixelPerTile*0.25 )

    currentPlayerPos = font.render(f"{cursorX} {cursorY} W: {selectedWall} T: {transparent}", True, (0,128,0), (220,220,220))
    window.blit(currentPlayerPos, (5,5))

    pg.display.flip()

    if pressed:
        time.sleep(0.3)
