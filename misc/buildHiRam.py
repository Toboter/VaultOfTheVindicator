import os
dirPath = "./hiram"

bankFiles = []

for file in os.listdir(dirPath):
    bankFiles.append(dirPath+"/"+file)

bankFiles.sort()

banks = b'\0\0'

for bankFile in bankFiles:
    with open(bankFile, "rb") as f:
        bank = f.read()
        if len(bank)<8192:
            bank += b'\0' * (8192-len(bank))
        banks += bank


with open("./build/world.bin", "wb") as f:
    f.write(banks)
