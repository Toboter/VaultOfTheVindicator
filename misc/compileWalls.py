import pygame as pg
import os


palette = pg.image.load("./graphics/cx16palette.png")

knownMappings = {}

def colorValueFromColor(color, blankColor=(255,0,255,255)):
    color = tuple(color)
    value = 0
    if color == blankColor:
        return b'\0'


    bestD = 200000
    ret = knownMappings.get(color)

    if ret:
        return ret

    for x in range(16):
        for y in range(16):
            if x==0 and y==0:
                continue
            c = palette.get_at((x*64, y*64))
            d = (color[0] - c[0])**2 + (color[1] - c[1])**2 + (color[2] - c[2])**2
            if d < bestD:
                bestD = d
                ret = y*16 + x

    ret = ret.to_bytes(1,"little")
    knownMappings[color] = ret

    return ret

def compileDir(dirPath, targetBank):
    tiles = []

    for file in os.listdir(dirPath):
        tiles.append(dirPath+"/"+file)

    tiles.sort()

    ret = b''

    for tile in tiles:
        img = pg.image.load(tile)
        for x in range(16):
            for y in range(16):
                ret += colorValueFromColor(img.get_at((x,y)))

    ret += b'\0'*(8192 - len(ret))

    with open(f"./hiram/{targetBank}.bank", "wb") as f:
        f.write(ret)


if __name__ == "__main__":
    compileDir("./graphics/solidWalls", 1)
    compileDir("./graphics/transparentWalls", 2)
