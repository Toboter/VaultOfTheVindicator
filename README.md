[comment]: # (SPDX-FileCopyrightText: 2023 Toboter)
[comment]: # (SPDX-License-Identifier: GPL-2.0-or-later)

# Vault of the Vindicator

A 3d roguelike for the Commander X16. Requires R44 or higher.

SPOILER WARNING: DON'T LOOK AT THE CODE OR GRAPHICS UNTIL YOU HAVE FINISHED THE GAME!

All code in this project is licensed under the included GPL-v2.0-or-later license. All graphics, sounds and music are licensed under CC BY-NC-SA 4.0.

A Game by Toboter

Music by Crisps

ZSMKit by MooingLemur is licensed under an MIT license (https://github.com/mooinglemur/zsmkit)

# How to compile
Make sure you have at least cc65 v2.19, Release 44 of x16emu and Python 3.8 with pygame 2.1.2 installed and in PATH
(if you're using a different python version, you must change compile.bat accordingly).
Then execute compile.bat.
If you're on Linux I guess just execute compile.bat in a shell of your choice, it should mostly still work
