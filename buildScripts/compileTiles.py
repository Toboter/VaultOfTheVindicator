# SPDX-FileCopyrightText: 2023 Toboter
#
# SPDX-License-Identifier: GPL-2.0-or-later

import pygame as pg


palette = pg.image.load("./graphics/cx16palette.png")

knownMappings = {}

def colorValueFromColor(color, blankColor=(255,0,255,255)):
    color = tuple(color)
    value = 0
    if color == blankColor:
        return b'\0'


    bestD = 200000
    ret = knownMappings.get(color)

    if ret:
        return ret

    for x in range(16):
        for y in range(16):
            if x==0 and y==0:
                continue
            c = palette.get_at((x*64, y*64))
            d = (color[0] - c[0])**2 + (color[1] - c[1])**2 + (color[2] - c[2])**2
            if d < bestD:
                bestD = d
                ret = y*16 + x

    ret = ret.to_bytes(1,"little")
    knownMappings[color] = ret

    return ret

def compileTileset(img, tileSize, tileCount):
    ret = b''
    tileX = 0
    tileY = 0
    for i in range(tileCount):
        for y in range(tileY,tileY+tileSize):
            for x in range(tileX,tileX+tileSize):
                ret += colorValueFromColor(img.get_at((x,y)), blankColor=(255,255,255, 255))
        tileX += tileSize
        if tileX >= img.get_width():
            tileX = 0
            tileY += tileSize

    return ret

def compileWallset(img, tileSize, tileCount):
    ret = b''
    tileX = 0
    tileY = 0
    for i in range(tileCount):
        for x in range(tileX,tileX+tileSize):
            for y in range(tileY,tileY+tileSize):
                ret += colorValueFromColor(img.get_at((x,y)), blankColor=(255,255,255, 255))
        tileX += tileSize
        if tileX >= img.get_width():
            tileX = 0
            tileY += tileSize

    return ret

def compileScreen(img, target):
    ret = b'\0\0'
    for y in range(120):
        for x in range(160):
            ret += colorValueFromColor(img.get_at((x,y)), blankColor=(255,255,255, 255))
        ret += b'\0'*160

    with open(target, "wb") as f:
        f.write(ret)


if __name__ == "__main__":
    #tile data starts at 0x13000
    ret = b'\0\0'
    ret += compileTileset(pg.image.load("./graphics/8x8tiles.png"), 8, 16*8) #0x13000 - 0x15000
    ret += compileTileset(pg.image.load("./graphics/16x16tiles.png"), 16, 8*4) #0x15000 - 0x17000

    with open("./build/graphics.bin", "wb") as f:
        f.write(ret)

    #wall data for hiram
    ret = b'\0\0'
    ret += compileWallset(pg.image.load("./graphics/solidWalls.png"), 16, 32) #bank 1
    ret += compileWallset(pg.image.load("./graphics/transparentWalls.png"), 16, 32) #bank2
    with open("./build/hiram.bin", "wb") as f:
        f.write(ret)

    #screens
    compileScreen(pg.image.load("./graphics/title.png"), "./build/title.scr");
    compileScreen(pg.image.load("./graphics/pause.png"), "./build/pause.scr");
    compileScreen(pg.image.load("./graphics/gameover.png"), "./build/gameover.scr");
    compileScreen(pg.image.load("./graphics/end1.png"), "./build/end1.scr");
    compileScreen(pg.image.load("./graphics/end2.png"), "./build/end2.scr");
    compileScreen(pg.image.load("./graphics/end3.png"), "./build/end3.scr");
    compileScreen(pg.image.load("./graphics/end4.png"), "./build/end4.scr");
