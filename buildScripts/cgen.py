# SPDX-FileCopyrightText: 2023 Toboter
#
# SPDX-License-Identifier: GPL-2.0-or-later


import struct
import math

class CDataType:
    def __init__(self, name, convertFunc):
        self.name = name
        self.convertFunc = convertFunc

class CList:
    def __init__(self, listName, dataType):
        self.listName = listName
        self.dataType = dataType
        self.data = []

    def addData(self, data):
        self.data.append(self.dataType.convertFunc(data))

    def getString(self):
        ret = f"{self.dataType.name} {self.listName}[{len(self.data)}] = "+"{"
        for d in self.data:
            ret += d+", "
        ret = ret[:-2] #remove last colon
        ret += "};"
        return ret

class CList2D:
    def __init__(self, listName, dataType):
        self.listName = listName
        self.dataType = dataType
        self.data = []

    def addData(self, data):
        self.data[-1].append(self.dataType.convertFunc(data))

    def nextDimension(self):
        self.data.append([])

    def getString(self):
        ret = f"{self.dataType.name} {self.listName}[{len(self.data)}][{len(self.data[0])}] = "+"{"
        for d1 in self.data:
            ret+="{"
            for d in d1:
                ret += d+", "
            ret = ret[:-2]+"}, "
        ret = ret[:-2] #remove last colon
        ret += "};"
        return ret


def _convertVeraFixed(f):
    assert f < 64
    f *= 512
    f = int(f)
    f &= 0b0111111111111111

    ret = struct.pack("<h", f)
    return hex(int.from_bytes(ret, "little"))




class CDataTypes:
    Uint8 = CDataType("uint8_t",lambda a: hex(int(round(a))%256)+"u")
    Uint16 = CDataType("uint16_t",lambda a: hex(int(round(a))%65536)+"u")
    VeraFixed = CDataType("uint16_t",lambda a: _convertVeraFixed(a))
