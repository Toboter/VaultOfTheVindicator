# SPDX-FileCopyrightText: 2023 Toboter
#
# SPDX-License-Identifier: GPL-2.0-or-later

from cgen import CDataTypes, CList, CList2D


screenWidth = 100 #cutoff 12 pixels
screenHeight = 100

wallPixelSize = 16

#wallHeightToPixelIncrement17_160 = CList("wallHeightToPixelIncrement17_160", CDataTypes.Uint8)
#index i = 16/i * 256 (format is 1/256, i in [17,160])
#for i in range(16,161):
#    incr = wallPixelSize/i*256
#    if incr>255:
#        incr = 255
#    wallHeightToPixelIncrement.addData(incr)

#wallHeightToPixelIncrement17_160 = CList("wallHeightToPixelIncrement17_160", CDataTypes.Uint8)

orthoWallWidthPixelIncrement = CList2D("orthoWallPixelIncrement", CDataTypes.Uint16)
#1/256 pixel, [dx (left)][y (+1)]
for x in range(0,10):
    orthoWallWidthPixelIncrement.nextDimension()
    for y in range(1,11):
        nx = x+0.5
        w = nx/(y+y*y)*screenWidth

        if w == 1:
            w =2

        incr = wallPixelSize/w*256
        if incr >= 256*wallPixelSize: #avoid gaps in very far away bit of wall
            incr = 256*10


        orthoWallWidthPixelIncrement.addData(incr)


wallGradient = CList("wallGradient", CDataTypes.Uint16)

grads = []
#1/256 pixel
for x in range(0,10):
    grad = screenHeight/((x+0.5)*screenWidth)*256
    grads.append(grad)
    #if grad < 256/3: #avoids rounding issues
        #grad *= 1.2
    wallGradient.addData(grad)

wallHeightToPixelIncrement = CList("wallHeightToPixelIncrement", CDataTypes.Uint16)
#1/256 pixel
#wallHeightToPixelIncrement.addData(1)

for x in range(1,190):
    incr = wallPixelSize/(x)*256
    wallHeightToPixelIncrement.addData(incr)


parallelWallWidths = CList("parallelWallSize", CDataTypes.Uint8)
wallWidths = []

for y in range(1,20):
    width = screenWidth/y
    wallWidths.append(width)
    parallelWallWidths.addData(width)

orthoWallWidths = CList2D("orthoWallWidths", CDataTypes.Uint8)
#1/256 pixel, [dx (left)][y (+1)]
for x in range(0,10):
    orthoWallWidths.nextDimension()
    for y in range(1,11):
        nx = x+0.5
        w = nx/(y+y*y)*screenWidth

        #print(round(w), (w*grads[x])//256, round(wallWidths[y]), round(wallWidths[y+1]))

        if w == 1:
            w =2
        orthoWallWidths.addData(w)


with open("src/render_tables.h", "w") as f:
    f.write(\
        f"""
        /*
         * SPDX-FileCopyrightText: 2023 Toboter
         *
         * SPDX-License-Identifier: GPL-2.0-or-later
         */
        #ifndef __RENDER_TABLES
        #define __RENDER_TABLES
        #include <stdint.h>

        {orthoWallWidthPixelIncrement.getString()}

        {wallGradient.getString()}

        {wallHeightToPixelIncrement.getString()}

        {parallelWallWidths.getString()}

        {orthoWallWidths.getString()}
        #endif
        """)
